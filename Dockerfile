FROM alpine:3.7

MAINTAINER Ejimba Eric <ejimba@gmail.com>

RUN apk upgrade -U && apk --update add \
  curl \
  gzip \
  git \
  mariadb-client \
  nginx \
  php7 \
  php7-ctype \
  php7-curl \
  php7-dom \
  php7-fileinfo \
  php7-fpm \
  php7-gd \
  php7-json \
  php7-mbstring \
  php7-openssl \
  php7-pdo \
  php7-pdo_mysql \
  php7-pdo_pgsql \
  php7-phar \
  php7-session \
  php7-tokenizer \
  php7-xml \
  php7-simplexml \
  php7-xmlwriter \
  php7-zip \
  php7-zlib \
  php7-mysqli \
  redis \
  supervisor && \
  rm -rf /var/cache/apk/*

RUN sed -i "s/request_terminate_timeout =.*/request_terminate_timeout=86400/g" /etc/php7/php.ini && \
  sed -i "s/default_socket_timeout =.*/default_socket_timeout=86400/g" /etc/php7/php.ini && \
  sed -i "s/max_input_time =.*/max_input_time=86400/g" /etc/php7/php.ini && \
  sed -i "s/max_execution_time =.*/max_execution_time=86400/g" /etc/php7/php.ini && \
  sed -i "s/upload_max_filesize =.*/upload_max_filesize=1G/g" /etc/php7/php.ini && \
  sed -i "s/post_max_size =.*/post_max_size=1G/g" /etc/php7/php.ini && \
  sed -i "s/memory_limit =.*/memory_limit=16G/g" /etc/php7/php.ini && \
  sed -i "s/cgi.fix_pathinfo=/cgi.fix_pathinfo=0#/g" /etc/php7/php.ini && \
  echo "daemon off;" >> /etc/nginx/nginx.conf && \
  mkdir -p /run/nginx

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --quiet

COPY config/nginx.conf /etc/nginx/conf.d/default.conf

COPY config/supervisor.conf /etc/supervisor.d/supervisor.ini

COPY . /var/www/html

WORKDIR /var/www/html

RUN touch crontab.tmp \
    && echo '* * * * * /usr/bin/php7 /var/www/html/artisan schedule:run >> /dev/null 2>&1' >> crontab.tmp \
    && crontab crontab.tmp \
    && rm -rf crontab.tmp

VOLUME ["/var/www/html"]

EXPOSE 80

CMD ["/usr/bin/supervisord"]