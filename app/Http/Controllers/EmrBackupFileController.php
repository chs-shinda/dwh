<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\EmrBackupFile;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class EmrBackupFileController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $emr_backup_files = \DB::select("
            SELECT
                facilities.code,
                facilities.name,
                facilities.county,
                facilities.sub_county,
                emr_types.name AS emr_type,
                last_visit_date AS last_updated
            FROM facilities
            LEFT JOIN (
                SELECT
                    emr_backup_files.emr_type_id,
                    emr_backup_files.facility_id,
                    emr_backup_files.last_visit_date,
                    ROW_NUMBER() OVER (PARTITION BY emr_backup_files.id ORDER BY emr_backup_files.created_at) r
                FROM emr_backup_files
                WHERE emr_backup_files.deleted_at IS NULL
            ) emr_backup_files ON facilities.id = emr_backup_files.facility_id AND emr_backup_files.r = 1
            LEFT JOIN emr_types ON emr_types.id = emr_backup_files.emr_type_id
            WHERE facilities.deleted_at IS NULL AND emr_types.deleted_at IS NULL
            ORDER BY facilities.county, facilities.sub_county
        ");

        if($request->input('export') == 'csv' || $request->input('export') == 'xls' || $request->input('export') == 'xlsx'|| $request->input('export') == 'pdf') {
            Excel::create('export_'.date('YmdHis'), function($excel) use ($emr_backup_files) {
                $excel->sheet('Sheet 1', function($sheet) use ($emr_backup_files) {
                    $sheet->fromArray(json_decode(json_encode($emr_backup_files), true));
                });
            })->export($request->input('export'));
        }
        else
        {
            return [
                'status' => 'OK',
                'emr_backup_files' => $emr_backup_files
            ];
        }
    }

    public function store(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'emr_type_id' => 'required',
            'facility_id' => 'required',
            'file' => 'required'
        ]);

        if ($v->fails()) {
            return [
                'status' => 'ERROR',
                'message' => 'There were validation errors. Please check and try again.',
                'errors' => $v->errors()->all()
            ];
        }

        $file = $request->file('file');

        $allowed_mimes = [
            'application/x-gzip',
            'application/zip',
            'text/plain'
        ];

        if(!in_array($file->getMimeType(), $allowed_mimes)) {
            return [
                'status' => 'ERROR',
                'message' => 'There were validation errors. Please check and try again.',
                'errors' => ['The file must be a file of type archive, zip or sql']
            ];
        }

        $destination_path = storage_path() . '/app/backups/';
        // $file_name = date('YmdHis').'_'.$file->getClientOriginalName();
        // $file_name = preg_replace('/\s+/', '_', $file_name);
        $file_name = $request->input('emr_type_id').'_'.$request->input('facility_id').'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();
        $file_name = str_replace(' ', '', $file_name);
        $request->file('file')->move($destination_path, $file_name);

        $obj = $request->all();
        $obj['backup_file'] = $destination_path.$file_name;
        $emr_backup_file = EmrBackupFile::create($obj);
        $emr_backup_files = EmrBackupFile::where('emr_type_id', $request->input('emr_type_id'))
                        ->where('facility_id', $request->input('facility_id'))
                        ->orderBy('created_at', 'DESC')->get();
        foreach ($emr_backup_files as $ebf) {
            if($ebf->id != $emr_backup_file->id) {
                $ebf->delete();
            }
        }

        $ebf = EmrBackupFile::with(['facility','emr_type'])->find($emr_backup_file->id);
        
        if($ebf) {
            if($ebf->emr_type->database_dialect == 'mysql') {
                \App\Jobs\RestoreMysqlEmrBackupFile::dispatch($ebf)->onQueue('restore_mysql_emr_backup_files');
            }
        }

        return [
            'status' => 'SUCCESS',
            'emr_backup_file' => $ebf
        ];
    }
}