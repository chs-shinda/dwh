<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class EmrDataController extends BaseController
{
    protected $types;

    public function __construct()
    {
        $this->middleware('auth');
        $this->types = [
            // 'facility.id' => '::integer',
            'facility.code' => '::character varying',
            'facility.name' => '::character varying',
            'facility.county' => '::character varying',
            'facility.sub_county' => '::character varying',
            'facility.ward' => '::character varying',
            'facility.constituency' => '::character varying',
            'facility.longitude' => '::character varying',
            'facility.latitude' => '::character varying',

            // 'patient.id' => '::integer',
            'patient.facility_id' => '::integer',
            'patient.emr_patient_id' => '::integer',
            'patient.ccc' => '::character varying',
            'patient.sex' => '::character varying',
            'patient.dob' => '::date',
            'patient.dob_estimated' => '::boolean',
            'patient.current_age' => '::integer',
            'patient.age_at_last_visit' => '::integer',
            'patient.marital_status' => '::character varying',
            'patient.education_level' => '::character varying',
            'patient.dead' => '::boolean',
            'patient.dod' => '::date',
            'patient.cause_of_death' => '::character varying',
            'patient.hiv_positive_confirmed_date' => '::date',
            'patient.hiv_positive_confirmed_facility' => '::character varying',
            'patient.registration_date' => '::date',
            'patient.hiv_enrolled_date' => '::date',
            'patient.tb_enrolled_date' => '::date',
            'patient.mch_child_enrolled_date' => '::date',
            'patient.mch_mother_enrolled_date' => '::date',
            'patient.art_start_date' => '::date',
            'patient.entry_point' => '::character varying',
            'patient.transfer_in_date' => '::date',
            'patient.transfer_in_facility' => '::character varying',
            'patient.transfer_in_district' => '::character varying',
            'patient.transfer_in_art_start_date' => '::date',
            'patient.first_visit_date' => '::date',
            'patient.last_visit_date' => '::date',
            'patient.initial_regimen' => '::character varying',
            'patient.initial_regimen_name' => '::character varying',
            'patient.initial_regimen_line' => '::character varying',
            'patient.initial_regimen_date' => '::date',
            'patient.current_regimen' => '::character varying',
            'patient.current_regimen_name' => '::character varying',
            'patient.current_regimen_line' => '::character varying',
            'patient.current_regimen_date' => '::date ',
            'patient.initial_who_stage' => '::character varying',
            'patient.initial_who_stage_date' => '::date',
            'patient.current_who_stage' => '::character varying',
            'patient.current_who_stage_date' => '::date',
            'patient.initial_cd4' => '::double precision',
            'patient.initial_cd4_date' => '::date',
            'patient.initial_cd4_percent' => '::double precision',
            'patient.initial_cd4_percent_date' => '::date',
            'patient.current_cd4' => '::double precision',
            'patient.current_cd4_date' => '::date',
            'patient.current_cd4_percent' => '::double precision',
            'patient.current_cd4_percent_date' => '::date',
            'patient.initial_vl' => '::double precision',
            'patient.initial_vl_date' => '::date',
            'patient.current_vl' => '::double precision',
            'patient.current_vl_date' => '::date',
            'patient.transfer_out_date' => '::date',
            'patient.transfer_out_facility' => '::character varying',
            'patient.next_appointment_date' => '::date',
            'patient.current_on_care' => '::boolean',
            'patient.current_on_art' => '::boolean',
            'patient.program_name' => '::character varying',

            // 'visit.id' => '::integer',
            // 'visit.patient_id' => '::integer',
            'visit.facility_id' => '::integer',
            'visit.emr_patient_id' => '::integer',
            'visit.emr_visit_id' => '::integer',
            'visit.visit_date' => '::date',
            'visit.visit_type' => '::character varying',
            'visit.encounter_type' => '::character varying',
            'visit.weight' => '::double precision',
            'visit.height' => '::double precision',
            'visit.systolic_pressure' => '::double precision',
            'visit.diastolic_pressure' => '::double precision',
            'visit.temperature' => '::double precision',
            'visit.pulse_rate' => '::double precision',
            'visit.respiratory_rate' => '::double precision',
            'visit.oxygen_saturation' => '::double precision',
            'visit.muac' => '::double precision',
            'visit.nutritional_status' => '::character varying',
            'visit.population_type' => '::character varying',
            'visit.key_population_type' => '::character varying',
            'visit.who_stage' => '::character varying',
            'visit.presenting_complaints' => '::text',
            'visit.clinical_notes' => '::text',
            'visit.on_anti_tb_drugs' => '::character varying',
            'visit.on_ipt' => '::character varying',
            'visit.ever_on_ipt' => '::character varying',
            'visit.spatum_smear_ordered' => '::character varying',
            'visit.chest_xray_ordered' => '::character varying',
            'visit.genexpert_ordered' => '::character varying',
            'visit.spatum_smear_result' => '::character varying',
            'visit.chest_xray_result' => '::character varying',
            'visit.genexpert_result' => '::character varying',
            'visit.referral' => '::character varying',
            'visit.clinical_tb_diagnosis' => '::character varying',
            'visit.contact_invitation' => '::character varying',
            'visit.evaluated_for_ipt' => '::character varying',
            'visit.has_known_allergies' => '::character varying',
            'visit.has_chronic_illnesses_cormobidities' => '::character varying',
            'visit.has_adverse_drug_reaction' => '::character varying',
            'visit.pregnancy_status' => '::character varying',
            'visit.wants_pregnancy' => '::character varying',
            'visit.pregnancy_outcome' => '::character varying',
            'visit.anc_number' => '::character varying',
            'visit.expected_delivery_date' => '::date',
            'visit.last_menstrual_period' => '::character varying',
            'visit.gravida' => '::character varying',
            'visit.parity' => '::character varying',
            'visit.full_term_pregnancies' => '::character varying',
            'visit.abortion_miscarriages' => '::character varying',
            'visit.family_planning_status' => '::character varying',
            'visit.family_planning_method' => '::character varying',
            'visit.reason_not_using_family_planning' => '::character varying',
            'visit.cough_for_2wks_or_more' => '::character varying',
            'visit.confirmed_tb_contact' => '::character varying',
            'visit.chronic_cough' => '::character varying',
            'visit.fever_for_2wks_or_more' => '::character varying',
            'visit.noticeable_weight_loss' => '::character varying',
            'visit.chest_pain' => '::character varying',
            'visit.night_sweat_for_2wks_or_more' => '::character varying',
            'visit.tb_status' => '::character varying',
            'visit.tb_treatment_no' => '::character varying',
            'visit.ctx_adherence' => '::character varying',
            'visit.ctx_dispensed' => '::character varying',
            'visit.dapsone_adherence' => '::character varying',
            'visit.dapsone_dispensed' => '::character varying',
            'visit.inh_dispensed' => '::character varying',
            'visit.regimen' => '::character varying',
            'visit.regimen_name' => '::character varying(191)',
            'visit.regimen_line' => '::character varying(191)',
            'visit.arv_adherence' => '::character varying',
            'visit.poor_arv_adherence_reason' => '::character varying',
            'visit.poor_arv_adherence_reason_other' => '::text',
            'visit.pwp_disclosure' => '::character varying',
            'visit.pwp_partner_tested' => '::character varying',
            'visit.condom_provided' => '::character varying',
            'visit.screened_for_sti' => '::character varying',
            'visit.cacx_screening' => '::character varying',
            'visit.cd4' => '::double precision',
            'visit.cd4_percent' => '::double precision',
            'visit.viral_load' => '::double precision',
            'visit.viral_load_ldl' => '::character varying(191)',
            'visit.sti_partner_notification' => '::character varying',
            'visit.at_risk_population' => '::character varying',
            'visit.next_appointment_date' => '::date',
            'visit.next_appointment_reason' => '::text',
            'visit.differentiated_care' => '::character varying',

            // 'visit.date_died' => '::date',
            // 'visit.to_facility' => '::character varying(191)',
            // 'visit.to_date' => '::date',
            // 'visit.program_name' => '::character varying(191)',
            
            // 'visit.reason_discontinued' => '::text',
            // 'visit.transfer_in_facility' => '::character varying(191)',
            // 'visit.transfer_in_district' => '::character varying(191)',
            // 'visit.transfer_in_art_start_date' => '::date',
            // 'visit.transfer_out_date' => '::date',
            // 'visit.transfer_out_facility' => '::character varying(191)',
        ];
    }

    public function index(Request $request)
    {
        $statement = "select ";
        $filter_object = [];
        foreach(explode('&', $_SERVER['QUERY_STRING']) as $part)
        {
            $part = explode('=', $part);
            if($key = array_shift($part))
            {
                $filter_object[ $key ] = implode('', $part);
            }
        }

        $types = $this->types;

        $filter_object_options = array_keys($filter_object);
        $special_options = ['export', 'fields', 'order_by', 'limit', 'skip', 'offset', 'api_key', 'type'];
        $filter_options_all = array_diff($filter_object_options, $special_options);
        $filter_options = array_intersect($filter_options_all, array_keys($types));
        $condition = '';
        $group_by = '';
        
        $facility_wheres = [];
        $patient_wheres = [];
        $visit_wheres = [];

        if(count($filter_options) > 0) {
            foreach ($filter_object as $key => $filter_obj) {
                if(in_array($key, $filter_options)) {
                    if(strpos($key, 'facility.') !== false) {
                        $facility_wheres[] = $key;
                    }
                    elseif(strpos($key, 'patient.') !== false) {
                        $patient_wheres[] = $key;
                    }
                    elseif(strpos($key, 'visit.') !== false) {
                        $visit_wheres[] = $key;
                    }
                    $condition_for_this_key = '';
                    $joiner = '';
                    if((strpos($filter_object[$key], '[') !== false) &&  (strpos($filter_object[$key], ']') !== false)) {
                        $condition_array = explode(',', str_replace(']', '', str_replace('[', '', trim($filter_object[$key]))));
                        if(count($condition_array) == 2) {
                            if(trim(urldecode($condition_array[0])) == "") {
                                $condition_array[0] = '=';
                            }
                            if((strpos($condition_array[1], '(') !== false) && (strpos($condition_array[1], ')') !== false)) {
                                $brackets_expression = '';
                                $bracket_array = explode(';', str_replace(')', '', str_replace('(', '', trim(urldecode($condition_array[1])))));
                                foreach ($bracket_array as $bracket_val) {
                                    $expression = "'".$bracket_val."'".$types[$key];
                                    $brackets_expression = $brackets_expression.$expression.',';
                                }
                                $brackets_expression = '('.rtrim($brackets_expression, ',').')';
                                $condition_for_this_key = $key.$types[$key]." ".urldecode($condition_array[0])." ".$brackets_expression;
                            }
                            else {
                                $condition_for_this_key = $key.$types[$key]." ".urldecode($condition_array[0])." '".urldecode($condition_array[1])."'".$types[$key];
                            }
                        }
                        else if(count($condition_array) == 3) {
                            if((trim(strtolower($condition_array[2]) == 'or')) || (trim(strtolower($condition_array[2]) == 'and'))) {
                                $joiner = strtolower($condition_array[2]);
                                if(trim(urldecode($condition_array[0])) == "") {
                                    $condition_array[0] = '=';
                                }
                                if((strpos($condition_array[1], '(') !== false) && (strpos($condition_array[1], ')') !== false)) {
                                    $brackets_expression = '';
                                    $bracket_array = explode(';', str_replace(')', '', str_replace('(', '', trim(urldecode($condition_array[1])))));
                                    foreach ($bracket_array as $bracket_val) {
                                        $expression = "'".$bracket_val."'".$types[$key];
                                        $brackets_expression = $brackets_expression.$expression.',';
                                    }
                                    $brackets_expression = '('.rtrim($brackets_expression, ',').')';
                                    $condition_for_this_key = $key.$types[$key]." ".urldecode($condition_array[0])." ".$brackets_expression;
                                }
                                else {
                                    $condition_for_this_key = $key.$types[$key]." ".urldecode($condition_array[0])." '".urldecode($condition_array[1])."'".$types[$key];
                                }
                            } else {
                                if(trim(urldecode($condition_array[0])) == "") {
                                    $condition_array[0] = '=';
                                }

                                $condition_for_this_key = $key.$types[$key]." ".urldecode($condition_array[0]);

                                if((strpos($condition_array[1], '(') !== false) && (strpos($condition_array[1], ')') !== false)) {
                                    $brackets_expression = '';
                                    $bracket_array = explode(';', str_replace(')', '', str_replace('(', '', trim(urldecode($condition_array[1])))));
                                    foreach ($bracket_array as $bracket_val) {
                                        $expression = "'".$bracket_val."'".$types[$key];
                                        $brackets_expression = $brackets_expression.$expression.',';
                                    }
                                    $brackets_expression = '('.rtrim($brackets_expression, ',').')';
                                    $condition_for_this_key = $condition_for_this_key." ".$brackets_expression." and ";
                                }
                                else {
                                    $condition_for_this_key = $condition_for_this_key." '".urldecode($condition_array[1])."'".$types[$key]." and ";
                                }

                                if((strpos($condition_array[2], '(') !== false) && (strpos($condition_array[2], ')') !== false)) {
                                    $brackets_expression = '';
                                    $bracket_array = explode(';', str_replace(')', '', str_replace('(', '', trim(urldecode($condition_array[2])))));
                                    foreach ($bracket_array as $bracket_val) {
                                        $expression = "'".$bracket_val."'".$types[$key];
                                        $brackets_expression = $brackets_expression.$expression.',';
                                    }
                                    $brackets_expression = '('.rtrim($brackets_expression, ',').')';
                                    $condition_for_this_key = $condition_for_this_key." ".$brackets_expression;
                                }
                                else {
                                    $condition_for_this_key = $condition_for_this_key." '".urldecode($condition_array[2])."'".$types[$key];
                                }
                            }
                        } else if(count($condition_array) == 4) {
                            if((trim(strtolower($condition_array[3])) == 'or') || (trim(strtolower($condition_array[3])) == 'and')) {
                                $joiner = trim(strtolower($condition_array[3]));
                                if(trim(urldecode($condition_array[0])) == "") {
                                    $condition_array[0] = '=';
                                }

                                $condition_for_this_key = $key.$types[$key]." ".urldecode($condition_array[0]);

                                if((strpos($condition_array[1], '(') !== false) && (strpos($condition_array[1], ')') !== false)) {
                                    $brackets_expression = '';
                                    $bracket_array = explode(';', str_replace(')', '', str_replace('(', '', trim(urldecode($condition_array[1])))));
                                    foreach ($bracket_array as $bracket_val) {
                                        $expression = "'".$bracket_val."'".$types[$key];
                                        $brackets_expression = $brackets_expression.$expression.',';
                                    }
                                    $brackets_expression = '('.rtrim($brackets_expression, ',').')';
                                    $condition_for_this_key = $condition_for_this_key." ".$brackets_expression." and ";
                                }
                                else {
                                    $condition_for_this_key = $condition_for_this_key." '".urldecode($condition_array[1])."'".$types[$key]." and ";
                                }

                                if((strpos($condition_array[2], '(') !== false) && (strpos($condition_array[2], ')') !== false)) {
                                    $brackets_expression = '';
                                    $bracket_array = explode(';', str_replace(')', '', str_replace('(', '', trim(urldecode($condition_array[2])))));
                                    foreach ($bracket_array as $bracket_val) {
                                        $expression = "'".$bracket_val."'".$types[$key];
                                        $brackets_expression = $brackets_expression.$expression.',';
                                    }
                                    $brackets_expression = '('.rtrim($brackets_expression, ',').')';
                                    $condition_for_this_key = $condition_for_this_key." ".$brackets_expression;
                                }
                                else {
                                    $condition_for_this_key = $condition_for_this_key." '".urldecode($condition_array[2])."'".$types[$key];
                                }
                            } else {
                                // "no joiner"
                            }
                        }
                        else {
                            continue;
                        }
                    }
                    else {
                        $condition_for_this_key = $key.$types[$key]." = '".urldecode($filter_object[$key])."'".$types[$key];
                    }
                    
                    if ($joiner == '') {
                        $condition = $condition." and ".$condition_for_this_key;
                    }
                    else {
                        $condition = $condition." ".$joiner." ".$condition_for_this_key;
                    }
                }
            }
        }

        $facility_fields = [];
        $patient_fields = [];
        $visit_fields = [];
        
        if($request->has('fields')) {
            $fields = explode(',', $request->input('fields'));
            $field_statement = '';
            foreach ($fields as $field) {
                if(strpos($field, 'facility.') !== false) {
                    $facility_fields[] = $field;
                }
                elseif(strpos($field, 'patient.') !== false) {
                    $patient_fields[] = $field;
                }
                elseif(strpos($field, 'visit.') !== false) {
                    $visit_fields[] = $field;
                }
            }
        }

        if((count($visit_fields) > 0) || (count($visit_wheres) > 0)) {
            if(trim(strtolower($request->input('type'))) == 'visits') {
                if($request->has('fields')) {
                    $field_statement = '';
                    foreach ($facility_fields as $field) {
                        if(strpos($field, '.') !== false) {
                            $field_statement = $field_statement.$field.' as '.str_replace('.','_',$field).',';
                        }
                        else {
                            $field_statement = $field_statement.$field.' as '.$field.',';
                        }
                    }
                    foreach ($patient_fields as $field) {
                        if(strpos($field, '.') !== false) {
                            $field_statement = $field_statement.$field.' as '.str_replace('.','_',$field).',';
                        }
                        else {
                            $field_statement = $field_statement.$field.' as '.$field.',';
                        }
                    }
                    foreach ($visit_fields as $field) {
                        if(strpos($field, '.') !== false) {
                            $field_statement = $field_statement.$field.' as '.str_replace('.','_',$field).',';
                        }
                        else {
                            $field_statement = $field_statement.$field.' as '.$field.',';
                        }
                    }
                    $statement = $statement.trim($field_statement, ',');
                }
                else {
                    $field_statement = '';
                    foreach (array_keys($types) as $field) {
                        if(strpos($field, '.') !== false) {
                            $field_statement = $field_statement.$field.' as '.str_replace('.','_',$field).',';
                        }
                        else {
                            $field_statement = $field_statement.$field.' as '.$field.',';
                        }
                    }
                    $statement = $statement.trim($field_statement, ',');
                }
            }
            else { // patients only
                if($request->has('fields')) {
                    $field_statement = '';
                    foreach ($facility_fields as $field) {
                        if(strpos($field, '.') !== false) {
                            $field_statement = $field_statement.'max('.$field.') as '.str_replace('.','_',$field).',';
                        }
                        else {
                            $field_statement = $field_statement.'max('.$field.') as '.$field.',';
                        }
                    }
                    foreach ($patient_fields as $field) {
                        if($field == 'patient.dob_estimated') {
                            $field_statement = $field_statement.'max(patient.dob_estimated::int) as patient_dob_estimated,';
                        }
                        elseif($field == 'patient.dead') {
                            $field_statement = $field_statement.'max(patient.dead::int) as patient_dead,';
                        }
                        elseif($field == 'patient.current_on_care') {
                            $field_statement = $field_statement.'max(patient.current_on_care::int) as patient_current_on_care,';
                        }
                        elseif($field == 'patient.current_on_art') {
                            $field_statement = $field_statement.'max(patient.current_on_art::int) as patient_current_on_art,';
                        }
                        elseif(strpos($field, '.') !== false) {
                            $field_statement = $field_statement.'max('.$field.') as '.str_replace('.','_',$field).',';
                        }
                        else {
                            $field_statement = $field_statement.'max('.$field.') as '.$field.',';
                        }
                    }
                    foreach ($visit_fields as $field) {
                        if(strpos($field, '.') !== false) {
                            $field_statement = $field_statement.'max('.$field.') as '.str_replace('.','_',$field).',';
                        }
                        else {
                            $field_statement = $field_statement.'max('.$field.') as '.$field.',';
                        }
                    }
                    $statement = $statement.trim($field_statement, ',');
                }
                else {
                    $field_statement = '';
                    foreach (array_keys($types) as $field) {
                        if(strpos($field, '.') !== false) {
                            if(strpos($field, 'visit.') !== false) {
                                $field_statement = $field_statement.'max('.$field.') as '.str_replace('.','_',$field).',';
                            }
                            else {
                                $field_statement = $field_statement.'max('.$field.') as '.str_replace('.','_',$field).',';
                            }
                        }
                        else {
                            if(strpos($field, 'visit.') !== false) {
                                $field_statement = $field_statement.'max('.$field.') as '.$field.',';
                            }
                            else {
                                $field_statement = $field_statement.'max('.$field.') as '.$field.',';
                            }
                        }
                    }
                    $statement = $statement.trim($field_statement, ',');
                }
                $group_by = ' visit.facility_id, visit.emr_patient_id ';
            }

            $statement = $statement." from visits as visit left join patients patient on visit.facility_id = patient.facility_id and visit.emr_patient_id = patient.emr_patient_id left join facilities facility on patient.facility_id = facility.id where facility.deleted_at is null ";
        }
        else {
            // doesnt have visit fields or visit wheres but has a visit type
            if(trim(strtolower($request->input('type'))) == 'visits') {
                if($request->has('fields')) {
                    $field_statement = '';
                    foreach ($facility_fields as $field) {
                        if(strpos($field, '.') !== false) {
                            $field_statement = $field_statement.$field.' as '.str_replace('.','_',$field).',';
                        }
                        else {
                            $field_statement = $field_statement.$field.' as '.$field.',';
                        }
                    }
                    foreach ($patient_fields as $field) {
                        if(strpos($field, '.') !== false) {
                            $field_statement = $field_statement.$field.' as '.str_replace('.','_',$field).',';
                        }
                        else {
                            $field_statement = $field_statement.$field.' as '.$field.',';
                        }
                    }
                    $statement = $statement.trim($field_statement, ',');
                }
                else {
                    $field_statement = '';
                    foreach (array_keys($types) as $field) {
                        if(strpos($field, '.') !== false) {
                            $field_statement = $field_statement.$field.' as '.str_replace('.','_',$field).',';
                        }
                        else {
                            $field_statement = $field_statement.$field.' as '.$field.',';
                        }
                    }
                    $statement = $statement.trim($field_statement, ',');
                }

                $statement = $statement." from visits as visit left join patients patient on visit.facility_id = patient.facility_id and visit.emr_patient_id = patient.emr_patient_id left join facilities facility on patient.facility_id = facility.id where facility.deleted_at is null ";
            }
            else {
                if($request->has('fields')) {
                    $field_statement = '';
                    foreach ($facility_fields as $field) {
                        if(strpos($field, '.') !== false) {
                            $field_statement = $field_statement.$field.' as '.str_replace('.','_',$field).',';
                        }
                        else {
                            $field_statement = $field_statement.$field.' as '.$field.',';
                        }
                    }
                    foreach ($patient_fields as $field) {
                        if(strpos($field, '.') !== false) {
                            $field_statement = $field_statement.$field.' as '.str_replace('.','_',$field).',';
                        }
                        else {
                            $field_statement = $field_statement.$field.' as '.$field.',';
                        }
                    }
                    $statement = $statement.trim($field_statement, ',');
                }
                else {
                    $field_statement = '';
                    foreach (array_keys($types) as $field) {
                        if(strpos($field, '.') !== false) {
                            if(strpos($field, 'visit.') !== false) {
                                continue;
                            }
                            else {
                                $field_statement = $field_statement.$field.' as '.str_replace('.','_',$field).',';
                            }
                        }
                        else {
                            if(strpos($field, 'visit.') !== false) {
                                continue;
                            }
                            else {
                                $field_statement = $field_statement.$field.' as '.$field.',';
                            }
                        }
                    }
                    $statement = $statement.trim($field_statement, ',');
                }

                $statement = $statement." from patients patient left join facilities facility on patient.facility_id = facility.id where facility.deleted_at is null ";
            }
        }

        $statement = $statement.$condition;

        if($group_by) {
            $statement = $statement." group by ".$group_by;
        }

        if($request->input('order_by')) {
            if($group_by) {
                $statement = $statement.", ".$request->input('order_by')." order by ".$request->input('order_by');
            }
            else {
                $statement = $statement." order by ".$request->input('order_by');
            }
        }

        if($request->input('limit')) {
            $statement = $statement." limit ".$request->input('limit');
        }

        if($request->input('skip')) {
            $statement = $statement." offset ".$request->input('skip');
        }

        if($request->input('offset')) {
            $statement = $statement." offset ".$request->input('offset');
        }

        $res = \DB::select($statement);

        if($request->input('export') == 'csv' || $request->input('export') == 'xls' || $request->input('export') == 'xlsx'|| $request->input('export') == 'pdf') {
            Excel::create('export_'.date('YmdHis'), function($excel) use ($res) {
                $excel->sheet('Sheet 1', function($sheet) use ($res) {
                    $sheet->fromArray(json_decode(json_encode($res), true));
                });
            })->export($request->input('export'));
        }
        else
        {
            return [
                'status' => 'OK',
                'emr_data' => $res
            ];
        }
    }

    public function fields(Request $request)
    {
        $all_fields = array_keys($this->types);

        if($request->has('type')) {
            $array = [];
            foreach($all_fields as $field) {
                if(strpos($field, $request->input('type').'.') !== false) {
                    $array[] = $field;
                }
            }
            return [
                'status' => 'OK',
                'fields' => $array
            ];
        }
        else {
            return [
                'status' => 'OK',
                'fields' => $all_fields
            ];
        }
    }

    public function summaries()
    {
        $summaries = \DB::connection('pgsql')->select("SELECT
                patient.facility_id,
                MAX(facility.code) AS facility_code,
                MAX(facility.name) AS facility_name,
                MAX(facility.county) AS facility_county,
                MAX(facility.sub_county) AS facility_sub_county,
                MAX(facility.ward) AS facility_ward,
                MAX(facility.constituency) AS facility_constituency,
                COUNT(patient.id) AS patients,
                COUNT(CASE WHEN current_age BETWEEN 0 AND 9 THEN 1 ELSE NULL END) AS age_0_9,
                COUNT(CASE WHEN current_age BETWEEN 10 AND 14 THEN 1 ELSE NULL END) AS age_10_14,
                COUNT(CASE WHEN current_age BETWEEN 15 AND 19 THEN 1 ELSE NULL END) AS age_15_19,
                COUNT(CASE WHEN current_age BETWEEN 20 AND 24 THEN 1 ELSE NULL END) AS age_20_24,
                COUNT(CASE WHEN current_age >= 25 THEN 1 ELSE NULL END) AS age_25_plus,
                COUNT(CASE WHEN ((current_age BETWEEN 0 AND 9) AND (patient.sex = 'male')) THEN 1 ELSE NULL END) AS age_0_9_male,
                COUNT(CASE WHEN ((current_age BETWEEN 0 AND 9) AND (patient.sex = 'female')) THEN 1 ELSE NULL END) AS age_0_9_female,
                COUNT(CASE WHEN ((current_age BETWEEN 10 AND 14) AND (patient.sex = 'male')) THEN 1 ELSE NULL END) AS age_10_14_male,
                COUNT(CASE WHEN ((current_age BETWEEN 10 AND 14) AND (patient.sex = 'female')) THEN 1 ELSE NULL END) AS age_10_14_female,
                COUNT(CASE WHEN ((current_age BETWEEN 15 AND 19) AND (patient.sex = 'male')) THEN 1 ELSE NULL END) AS age_15_19_male,
                COUNT(CASE WHEN ((current_age BETWEEN 15 AND 19) AND (patient.sex = 'female')) THEN 1 ELSE NULL END) AS age_15_19_female,
                COUNT(CASE WHEN ((current_age BETWEEN 20 AND 24) AND (patient.sex = 'male')) THEN 1 ELSE NULL END) AS age_20_24_male,
                COUNT(CASE WHEN ((current_age BETWEEN 20 AND 24) AND (patient.sex = 'female')) THEN 1 ELSE NULL END) AS age_20_24_female,
                COUNT(CASE WHEN ((current_age >= 25) AND (patient.sex = 'male')) THEN 1 ELSE NULL END) AS age_25_plus_male,
                COUNT(CASE WHEN ((current_age >= 25) AND (patient.sex = 'female')) THEN 1 ELSE NULL END) AS age_25_plus_female,
                COUNT(CASE WHEN patient.dead = true THEN 1 ELSE NULL END) AS dead,
                COUNT(CASE WHEN patient.sex = 'male' THEN 1 ELSE NULL END) AS male,
                COUNT(CASE WHEN patient.sex = 'female' THEN 1 ELSE NULL END) AS female,
                COUNT(patient.hiv_enrolled_date) AS hiv_enrolled,
                COUNT(patient.art_start_date) AS art_started,
                COUNT(patient.transfer_in_date) AS transfer_in,
                COUNT(patient.transfer_out_date) AS transfer_out,
                MAX(last_visit_date) AS last_visit_date,
                COUNT(current_vl) AS uptake,
                COUNT(CASE WHEN patient.current_vl < 1000 THEN 1 ELSE NULL END) AS suppressed,
                COUNT(CASE WHEN date_part('day',age(now(), patient.last_visit_date)) >= 90 THEN 1 ELSE NULL END) AS ltfu
            FROM patients patient
            LEFT JOIN facilities facility ON facility.id = patient.facility_id
            WHERE patient.current_on_care = true
            GROUP BY patient.facility_id"
        );
        return [
            'status' => 'OK',
            'summaries' => $summaries
        ];
    }
}