<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\EmrType;

class EmrTypeController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $emr_types = EmrType::orderBy('id')->get();
        return [
            'status' => 'OK',
            'emr_types' => $emr_types
        ];
    }

    public function store(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($v->fails()) {
            return [
                'status' => 'ERROR',
                'message' => 'There were validation errors. Please check and try again.',
                'errors' => $v->errors()->all()
            ];
        }

        $emr_type = EmrType::create($request->all());

        return [
            'status' => 'SUCCESS',
            'emr_type' => $emr_type
        ];
    }
}