<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Facility;

class FacilityController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $facilities = Facility::orderBy('name')->get();
        return [
            'status' => 'OK',
            'facilities' => $facilities
        ];
    }

    public function store(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required',
        ]);

        if ($v->fails()) {
            return [
                'status' => 'ERROR',
                'message' => 'There were validation errors. Please check and try again.',
                'errors' => $v->errors()->all()
            ];
        }

        $facility = Facility::create($request->all());

        return [
            'status' => 'SUCCESS',
            'facility' => $facility
        ];
    }

    public function import(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'file' => 'required'
        ]);

        if ($v->fails()) {
            return [
                'status' => 'ERROR',
                'message' => 'There were validation errors. Please check and try again.',
                'errors' => $v->errors()->all()
            ];
        }

        $file = $request->file('file');

        $destination_path = storage_path() . '/app/uploads/';
        $file_name = date('YmdHis').'_'.$file->getClientOriginalName();
        $file_name = preg_replace('/\s+/', '_', $file_name);
        $request->file('file')->move($destination_path, $file_name);

        $rows = \Excel::load($destination_path.$file_name, function($reader) {
            $reader->toArray();
        })->get();

        $records = 0;
        $errors = 0;

        foreach ($rows as $row)
        {
            $facility = Facility::where('code', $row->code)->first();
            $obj = json_decode(json_encode($row),TRUE); 
            if ($facility) {
                $facility->update($obj);
            } else {
                Facility::create($obj);
            }
            $records = $records + 1;
        }

        if($errors > 0) {
            return [
                'status' => 'ERROR',
                'message' => 'Errors occured during import. '.$records.' records imported. Please check the records.',
            ];
        }
        else {
            return [
                'status' => 'OK',
                'message' => 'Import processed Successfully. '.$records.' records imported.'
            ];
        }
    }
}