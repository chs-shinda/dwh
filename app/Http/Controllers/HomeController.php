<?php

namespace App\Http\Controllers;

class HomeController extends BaseController
{
    protected $emr_backup_file;

    public function __construct()
    {
        $this->middleware('auth', ['except' => 'index']);
    }

    public function index()
    {
        return ['status' => 'OK', 'message'=>'CHS-SHINDA DWH API'];
    }

    public function status()
    {
        $queue = \App::make('queue.connection');
        
        $restore_mysql_emr_backup_files_queue = $queue->size('restore_mysql_emr_backup_files');
        $create_kenya_emr_etl_queue = $queue->size('create_kenya_emr_etl');
        $get_patient_profiles_queue = $queue->size('get_patient_profiles');
        $get_patient_visits_queue = $queue->size('get_patient_visits');
        $get_patient_summaries_queue = $queue->size('get_patient_summaries');
        $default_queue = $queue->size('default');

        $patients = \App\Models\Patient::count();
        $visits = \App\Models\Visit::count();

        return [
            'patients' => $patients,
            'visits' => $visits,
            'restore_mysql_emr_backup_files_queue' => $restore_mysql_emr_backup_files_queue,
            'create_kenya_emr_etl_queue' => $create_kenya_emr_etl_queue,
            'get_patient_profiles_queue' => $get_patient_profiles_queue,
            'get_patient_visits_queue' => $get_patient_visits_queue,
            'get_patient_summaries_queue' => $get_patient_summaries_queue,
            'default_queue' => $default_queue
        ];
    }

    public function process()
    {
        $emr_backup_files = \App\Models\EmrBackupFile::whereNotNull('database_name')
                            // ->where('facility_id', '=', 21)
                            ->get();
        foreach ($emr_backup_files as $emr_backup_file) {
            // \App\Models\Visit::where('facility_id', $emr_backup_file->facility_id)->delete();
            // \App\Jobs\GetPatientVisitsKenyaEMR::dispatch($emr_backup_file)->onQueue('get_patient_visits');
            \App\Jobs\DeIdentifyKenyaEMRDataTool::dispatch($emr_backup_file);
        }
        return ['status' => 'OK', 'message'=> 'Success'];
    }

    public function clean()
    {
        $emr_backup_files = \App\Models\EmrBackupFile::onlyTrashed()->get();
        foreach ($emr_backup_files as $emr_backup_file) {
            \App\Jobs\DeletePreviousEmrBackupFileKenyaEMR::dispatch($emr_backup_file);
        }
        return ['status' => 'OK', 'message'=> 'Success'];
    }
}