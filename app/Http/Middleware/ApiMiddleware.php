<?php

namespace App\Http\Middleware;

use Closure;

class ApiMiddleware
{
    public function handle($request, Closure $next)
    {
        if($request->api_key !== 'd0cedebda217433a80b3ea4f08e0271b') {
            return response([
                'status' => 'ERROR',
                'message' => 'Access Denied'
            ]);
        }
        return $next($request);
    }
}