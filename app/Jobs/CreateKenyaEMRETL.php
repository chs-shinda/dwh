<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Symfony\Component\Process\Process;

class CreateKenyaEMRETL implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emr_backup_file;

    public function __construct(\App\Models\EmrBackupFile $emr_backup_file)
    {
        $this->emr_backup_file = $emr_backup_file;
    }

    public function handle()
    {
        $ddl_path = base_path().'/resources/sql/v17.1.0/DDL.sql';
        $dml_path = base_path().'/resources/sql/v17.1.0/DML.sql';
        $chs_path = base_path().'/resources/sql/v17.3.0/CHS.sql';
        $dt_path = base_path().'/resources/sql/v17.1.0/DataTools.sql';

        $ddl_file = storage_path().'/app/uploads/'.$this->emr_backup_file->database_name.'_etl_ddl.sql';
        $dml_file = storage_path().'/app/uploads/'.$this->emr_backup_file->database_name.'_etl_dml.sql';
        $dt_file = storage_path().'/app/uploads/'.$this->emr_backup_file->database_name.'_etl_dt.sql';
        $chs_file = storage_path().'/app/uploads/'.$this->emr_backup_file->database_name.'_etl_chs.sql';

        if(isset($this->emr_backup_file->emr_type->name) && $this->emr_backup_file->emr_type->name == 'KenyaEMR' && \File::exists($ddl_path) && \File::exists($dml_path) && \File::exists($chs_path) && \File::exists($dt_path)) {

            $ddl = \File::get($ddl_path);
            $dml = \File::get($dml_path);
            $chs = \File::get($chs_path);
            $dt = \File::get($dt_path);

            $ddl = str_replace('kenyaemr_etl', $this->emr_backup_file->database_name.'_etl', $ddl);
            $dml = str_replace('kenyaemr_etl', $this->emr_backup_file->database_name.'_etl', $dml);
            //$chs = str_replace('kenyaemr_etl', $this->emr_backup_file->database_name.'_etl', $chs);
            $dt = str_replace('kenyaemr_datatools', $this->emr_backup_file->database_name.'_dt', $dt);
            $dt = str_replace('kenyaemr_etl', $this->emr_backup_file->database_name.'_etl', $dt);

            $dt = $dt."
                CALL create_datatools_tables();
            ";

            \File::put($ddl_file, $ddl);
            \File::put($dml_file, $dml);
            \File::put($chs_file, $chs);
            \File::put($dt_file, $dt);

            $command = sprintf('mysql --host=%s --port=%s --user=%s --password=%s %s < %s',
                escapeshellarg(\Config::get('database.connections.pl_mysql.host')),
                escapeshellarg(\Config::get('database.connections.pl_mysql.port')),
                escapeshellarg(\Config::get('database.connections.pl_mysql.username')),
                escapeshellarg(\Config::get('database.connections.pl_mysql.password')),
                escapeshellarg($this->emr_backup_file->database_name),
                escapeshellarg($ddl_file)
            );

            try {
                $process = new Process($command);
                $process->setTimeout(null);
                $process->disableOutput();
                $process->mustRun();

                if($process->isSuccessful())
                {
                    $command2 = sprintf('mysql --host=%s --port=%s --user=%s --password=%s %s < %s',
                        escapeshellarg(\Config::get('database.connections.pl_mysql.host')),
                        escapeshellarg(\Config::get('database.connections.pl_mysql.port')),
                        escapeshellarg(\Config::get('database.connections.pl_mysql.username')),
                        escapeshellarg(\Config::get('database.connections.pl_mysql.password')),
                        escapeshellarg($this->emr_backup_file->database_name),
                        escapeshellarg($dml_file)
                    );

                    $process2 = new Process($command2);
                    $process2->setTimeout(null);
                    $process2->disableOutput();
                    $process2->mustRun();

                    if($process2->isSuccessful())
                    {
                        $command3 = sprintf('mysql --host=%s --port=%s --user=%s --password=%s %s < %s',
                            escapeshellarg(\Config::get('database.connections.pl_mysql.host')),
                            escapeshellarg(\Config::get('database.connections.pl_mysql.port')),
                            escapeshellarg(\Config::get('database.connections.pl_mysql.username')),
                            escapeshellarg(\Config::get('database.connections.pl_mysql.password')),
                            escapeshellarg($this->emr_backup_file->database_name),
                            escapeshellarg($dt_file)
                        );

                        $process3 = new Process($command3);
                        $process3->setTimeout(null);
                        $process3->disableOutput();
                        $process3->mustRun();

                        if($process3->isSuccessful())
                    {
                        $command4 = sprintf('mysql --host=%s --port=%s --user=%s --password=%s %s < %s',
                            escapeshellarg(\Config::get('database.connections.pl_mysql.host')),
                            escapeshellarg(\Config::get('database.connections.pl_mysql.port')),
                            escapeshellarg(\Config::get('database.connections.pl_mysql.username')),
                            escapeshellarg(\Config::get('database.connections.pl_mysql.password')),
                            escapeshellarg($this->emr_backup_file->database_name),
                            escapeshellarg($chs_file)
                        );

                        $process4 = new Process($command4);
                        $process4->setTimeout(null);
                        $process4->disableOutput();
                        $process4->mustRun();

                        if($process4->isSuccessful())
                        {
                            \File::delete($ddl_file);
                            \File::delete($dml_file);
                            \File::delete($chs_file);
                            \File::delete($dt_file);

                            \App\Jobs\DeletePreviousPatientVisits::dispatch($this->emr_backup_file)->onQueue('delete_previous_patient_visits');
                        }
                    }
                    }
                }
            } catch(\Exception $e) {
                info($e->getMessage());
                \App\Jobs\CreateKenyaEMRETLV2::dispatch($this->emr_backup_file)->onQueue('create_kenya_emr_etl');
            }
        }
    }
}
