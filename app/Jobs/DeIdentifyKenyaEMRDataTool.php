<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Symfony\Component\Process\Process;
use Faker\Factory;

class DeIdentifyKenyaEMRDataTool implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $database_name;

    public function __construct($database_name)
    {
        $this->database_name = $database_name;
    }

    public function handle()
    {
        if(isset($this->database_name)) {

            $host = \Config::get('database.connections.hq_mysql.host');
            $database = $this->database_name;
            $username = \Config::get('database.connections.hq_mysql.username');
            $password = \Config::get('database.connections.hq_mysql.password');

            
            $connection = new \PDO("mysql:host=$host;dbname=$database", $username,$password, [ \PDO::ATTR_PERSISTENT => true ]);

            $query_mysql = "SELECT * FROM patient_demographics ORDER BY patient_id";
            $statement = $connection->prepare($query_mysql, [ \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY ]);

            if ($statement->execute()) {
                while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
                    $faker = Factory::create();
                    $gender = $row['Gender'] == 'M' ? 'male':'female';
                    $ts_gender = $row['Gender'] == 'M' ? 'female':'male';

                    $data = [];

                    $data['given_name'] = $faker->firstName($gender);
                    $data['middle_name'] = $faker->firstName($gender);
                    $data['family_name'] = $faker->lastName($gender);
                    $data['national_id_no'] = null;
                    $data['unique_patient_no'] = $faker->isbn10();
                    $data['patient_clinic_number'] = substr($data['unique_patient_no'], 5, 5);
                    $data['Tb_no'] = null;
                    $data['district_reg_no'] = null;
                    $data['hei_no'] = null;
                    $data['phone_number'] = $faker->e164PhoneNumber();
                    $data['email_address'] = $faker->safeEmail();
                    $data['next_of_kin'] = $faker->name($ts_gender);
                    $data['patient_id'] = $row['patient_id'];

                    $update_mysql = "
                        UPDATE patient_demographics SET 
                            given_name = :given_name,
                            middle_name = :middle_name,
                            family_name = :family_name,
                            national_id_no = :national_id_no,
                            unique_patient_no = :unique_patient_no,
                            patient_clinic_number = :patient_clinic_number,
                            Tb_no = :Tb_no,
                            district_reg_no = :district_reg_no,
                            hei_no = :hei_no,
                            phone_number = :phone_number,
                            email_address = :email_address,
                            next_of_kin = :next_of_kin
                        WHERE patient_id = :patient_id";

                    $connection->prepare($update_mysql)->execute($data);
                }
            }

            $query_mysql = "SELECT * FROM hiv_enrollment ORDER BY patient_id";
            $statement = $connection->prepare($query_mysql, [ \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY ]);

            if ($statement->execute()) {
                while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
                    $faker = Factory::create();
                    $gender = $faker->randomElements(['male', 'female'])[0];
                    $data = [];
                    
                    $data['name_of_treatment_supporter'] = $faker->name($gender);
                    $data['treatment_supporter_telephone'] = $faker->e164PhoneNumber();
                    $data['treatment_supporter_address'] = $faker->address();
                    $data['patient_id'] = $row['patient_id'];

                    $update_mysql = "
                        UPDATE hiv_enrollment SET 
                            name_of_treatment_supporter = :name_of_treatment_supporter,
                            treatment_supporter_telephone = :treatment_supporter_telephone,
                            treatment_supporter_address = :treatment_supporter_address
                        WHERE patient_id = :patient_id";

                    $connection->prepare($update_mysql)->execute($data);
                }
            }

            $query_mysql = "SELECT * FROM tb_enrollment ORDER BY patient_id";
            $statement = $connection->prepare($query_mysql, [ \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY ]);

            if ($statement->execute()) {
                while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
                    $faker = Factory::create();
                    $gender = $faker->randomElements(['male', 'female'])[0];
                    $data = [];
                    
                    $data['treatment_supporter'] = $faker->name($gender);
                    $data['treatment_supporter_phone_contact'] = $faker->e164PhoneNumber();
                    $data['treatment_supporter_address'] = $faker->address();
                    $data['patient_id'] = $row['patient_id'];

                    $update_mysql = "
                        UPDATE tb_enrollment SET 
                            treatment_supporter = :treatment_supporter,
                            treatment_supporter_phone_contact = :treatment_supporter_phone_contact,
                            treatment_supporter_address = :treatment_supporter_address
                        WHERE patient_id = :patient_id";

                    $connection->prepare($update_mysql)->execute($data);
                }
            }
        }
    }
}