<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Symfony\Component\Process\Process;

class DeletePreviousEmrBackupFileKenyaEMR implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emr_backup_file;

    public function __construct(\App\Models\EmrBackupFile $emr_backup_file)
    {
        $this->emr_backup_file = $emr_backup_file;
    }

    public function handle()
    {
        if(isset($this->emr_backup_file->emr_type->name) && $this->emr_backup_file->emr_type->name == 'KenyaEMR' && $this->emr_backup_file->database_name) {

            $database_name = $this->emr_backup_file->database_name;

            $command = sprintf('mysql --protocol=tcp --host=%s --port=%s --user=%s --password=%s -e "drop database if exists %s"',
                escapeshellarg(\Config::get('database.connections.pl_mysql.host')),
                escapeshellarg(\Config::get('database.connections.pl_mysql.port')),
                escapeshellarg(\Config::get('database.connections.pl_mysql.username')),
                escapeshellarg(\Config::get('database.connections.pl_mysql.password')),
                $database_name
            );

            $process = new Process($command);
            $process->setTimeout(null);
            $process->disableOutput();
            $process->mustRun();

            if($process->isSuccessful())
            {
                $database_name2 = $database_name.'_etl';

                $command2 = sprintf('mysql --protocol=tcp --host=%s --port=%s --user=%s --password=%s -e "drop database if exists %s"',
                    escapeshellarg(\Config::get('database.connections.pl_mysql.host')),
                    escapeshellarg(\Config::get('database.connections.pl_mysql.port')),
                    escapeshellarg(\Config::get('database.connections.pl_mysql.username')),
                    escapeshellarg(\Config::get('database.connections.pl_mysql.password')),
                    $database_name2
                );

                $process2 = new Process($command2);
                $process2->setTimeout(null);
                $process2->disableOutput();
                $process2->mustRun();

                if($process2->isSuccessful())
                {
                    $database_name3 = $database_name.'_dt';

                    $command3 = sprintf('mysql --protocol=tcp --host=%s --port=%s --user=%s --password=%s -e "drop database if exists %s"',
                        escapeshellarg(\Config::get('database.connections.pl_mysql.host')),
                        escapeshellarg(\Config::get('database.connections.pl_mysql.port')),
                        escapeshellarg(\Config::get('database.connections.pl_mysql.username')),
                        escapeshellarg(\Config::get('database.connections.pl_mysql.password')),
                        $database_name3
                    );

                    $process3 = new Process($command3);
                    $process3->setTimeout(null);
                    $process3->disableOutput();
                    $process3->mustRun();

                    if($process3->isSuccessful()) {

                        
                    }
                }
            }
        }
    }
}