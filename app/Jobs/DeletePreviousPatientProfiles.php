<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DeletePreviousPatientProfiles implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emr_backup_file;

    public function __construct(\App\Models\EmrBackupFile $emr_backup_file)
    {
        $this->emr_backup_file = $emr_backup_file;
    }

    public function handle()
    {
        \App\Models\Patient::where('facility_id', $this->emr_backup_file->facility_id)->delete();

        if(isset($this->emr_backup_file->emr_type->name) && $this->emr_backup_file->emr_type->name == 'KenyaEMR')
        {
            \App\Jobs\GetPatientProfilesKenyaEMR::dispatch($this->emr_backup_file)->onQueue('get_patient_profiles');
        }
        elseif(isset($this->emr_backup_file->emr_type->name) && $this->emr_backup_file->emr_type->name == 'OpenMRS')
        {
            \App\Jobs\GetPatientProfilesOpenMRS::dispatch($this->emr_backup_file)->onQueue('get_patient_profiles');
        }
    }
}