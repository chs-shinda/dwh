<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GetCurrentKenyaEMR implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emr_backup_file;

    public function __construct(\App\Models\EmrBackupFile $emr_backup_file)
    {
        $this->emr_backup_file = $emr_backup_file;
    }

    public function handle()
    {
        if($this->emr_backup_file->last_visit_date && $this->emr_backup_file->emr_type->name == 'KenyaEMR') {

            $last_visit_date = \Carbon\Carbon::createFromFormat('Y-m-d', $this->emr_backup_file->last_visit_date);
            $start_date = $last_visit_date->subMonth()->firstOfMonth()->format('Y-m-d');
            $end_date = $last_visit_date->lastOfMonth()->format('Y-m-d');
            
            $query_mysql = "SELECT
                fup.visit_date,
                fup.patient_id,
                p.dob,
                p.Gender,
                min(e.visit_date) as enroll_date,
                max(fup.visit_date) as latest_vis_date,
                mid(max(concat(fup.visit_date,fup.next_appointment_date)),11) as latest_tca,
                p.unique_patient_no,
                max(d.visit_date) as date_discontinued,
                d.patient_id as disc_patient,
                de.patient_id as started_on_drugs
            from etl_patient_hiv_followup fup
            join etl_patient_demographics p on p.patient_id=fup.patient_id
            join etl_hiv_enrollment e on fup.patient_id=e.patient_id
            left outer join etl_drug_event de on e.patient_id = de.patient_id and date(date_started) <= :endDate
            left outer JOIN (
                select
                    patient_id,
                    visit_date
                from etl_patient_program_discontinuation
                where date(visit_date) <= :endDate and program_name='HIV'
                group by patient_id
            ) d on d.patient_id = fup.patient_id
            where fup.visit_date <= :endDate
            group by patient_id
            having (
                (date(latest_tca) > :endDate and (date(latest_tca) > date(date_discontinued) or disc_patient is null )) or
                (
                    ((date(latest_tca) between :startDate and :endDate) and (date(latest_vis_date) >= date(latest_tca))) and
                    (date(latest_tca) > date(date_discontinued) or disc_patient is null)
                )
            )";

            $host = \Config::get('database.connections.pl_mysql.host');
            $database = $this->emr_backup_file->database_name.'_etl';
            $username = \Config::get('database.connections.pl_mysql.username');
            $password = \Config::get('database.connections.pl_mysql.password');

            $connection = new \PDO("mysql:host=$host;dbname=$database", $username,$password, [ \PDO::ATTR_PERSISTENT => true ]);
            $statement = $connection->prepare($query_mysql, [ \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY ]);
            $statement->bindParam(':endDate', $start_date); 
            $statement->bindParam(':startDate', $end_date);

            if ($statement->execute()) {
                while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
                    \App\Models\Patient::where('facility_id', $this->emr_backup_file->facility_id)
                                        ->where('emr_patient_id', $row['patient_id'])
                                        ->update(['current_on_care' => true]);
                }
            }

            \App\Jobs\UpdatePatientAgesArt::dispatch($this->emr_backup_file);
        }
    }
}