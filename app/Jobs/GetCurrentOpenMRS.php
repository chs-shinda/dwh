<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GetCurrentOpenMRS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emr_backup_file;

    public function __construct(\App\Models\EmrBackupFile $emr_backup_file)
    {
        $this->emr_backup_file = $emr_backup_file;
    }

    public function handle()
    {
        if($this->emr_backup_file->last_visit_date && $this->emr_backup_file->emr_type->name == 'OpenMRS') {

            $last_visit_date = \Carbon\Carbon::createFromFormat('Y-m-d', $this->emr_backup_file->last_visit_date);
            $start_date = $last_visit_date->subMonth()->firstOfMonth()->format('Y-m-d');
            $end_date = $last_visit_date->lastOfMonth()->format('Y-m-d');
            
            // $query_mysql = "";

            // $host = \Config::get('database.connections.pl_mysql.host');
            // $database = $this->emr_backup_file->database_name.'_etl';
            // $username = \Config::get('database.connections.pl_mysql.username');
            // $password = \Config::get('database.connections.pl_mysql.password');

            // $connection = new \PDO("mysql:host=$host;dbname=$database", $username,$password, [ \PDO::ATTR_PERSISTENT => true ]);
            // $statement = $connection->prepare($query_mysql, [ \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY ]);
            // $statement->bindParam(':endDate', $start_date); 
            // $statement->bindParam(':startDate', $end_date);

            // if ($statement->execute()) {
            //     while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
            //         \App\Models\Patient::where('facility_id', $this->emr_backup_file->facility_id)
            //                             ->where('emr_patient_id', $row['patient_id'])
            //                             ->update(['current_on_care' => true]);
            //     }
            // }

            \App\Jobs\UpdatePatientAgesArt::dispatch($this->emr_backup_file);
        }
    }
}