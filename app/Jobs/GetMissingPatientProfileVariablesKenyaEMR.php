<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GetMissingPatientProfileVariablesKenyaEMR implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emr_backup_file;

    public function __construct(\App\Models\EmrBackupFile $emr_backup_file)
    {
        $this->emr_backup_file = $emr_backup_file;
    }

    public function handle()
    {
        $query_mysql = "SELECT
            a.person_id AS emr_patient_id,
            a.birthdate_estimated AS dob_estimated,
            CASE WHEN h.hiv_enrolled_date IS NULL THEN h.hiv_enrolled_date ELSE DATE_FORMAT(h.hiv_enrolled_date,'%Y-%m-%d') END AS hiv_enrolled_date,
            CASE WHEN h.mch_child_enrolled_date IS NULL THEN h.mch_child_enrolled_date ELSE DATE_FORMAT(h.mch_child_enrolled_date,'%Y-%m-%d') END AS mch_child_enrolled_date,
            CASE WHEN h.mch_mother_enrolled_date IS NULL THEN h.mch_mother_enrolled_date ELSE DATE_FORMAT(h.mch_mother_enrolled_date,'%Y-%m-%d') END AS mch_mother_enrolled_date,
            CASE WHEN h.registration_date IS NULL THEN h.registration_date ELSE DATE_FORMAT(h.registration_date,'%Y-%m-%d') END AS registration_date,
            CASE WHEN h.tb_enrolled_date IS NULL THEN h.tb_enrolled_date ELSE DATE_FORMAT(h.tb_enrolled_date,'%Y-%m-%d') END AS tb_enrolled_date,
            i.cause_of_death
        FROM person a
        INNER JOIN patient b ON b.patient_id = a.person_id
        LEFT JOIN (
            SELECT
                a.patient_id,
                max(if(b.uuid='05ee9cf4-7242-4a17-b4d4-00f707265c8a',lower(a.identifier),null)) AS upn,
                max(if(b.uuid='d8ee3b8c-a8fc-4d6b-af6a-9423be5f8906',lower(a.identifier),null)) AS district_reg_number,
                max(if(b.uuid='c4e3caca-2dcc-4dc4-a8d9-513b6e63af91',lower(a.identifier),null)) AS tb_treatment_number,
                max(if(b.uuid='b4d66522-11fc-45c7-83e3-39a1af21ae0d',lower(a.identifier),null)) AS patient_clinic_number,
                max(if(b.uuid='49af6cdc-7968-4abb-bf46-de10d7f4859f',lower(a.identifier),null)) AS national_id,
                max(if(b.uuid='0691f522-dd67-4eeb-92c8-af5083baf338',lower(a.identifier),null)) AS hei_id
            FROM patient_identifier a
            JOIN patient_identifier_type b ON a.identifier_type=b.patient_identifier_type_id
            WHERE voided = 0
            GROUP BY a.patient_id  
        ) e ON e.patient_id = b.patient_id
        LEFT JOIN (
            SELECT
                patient_id,
                max(if(b.name='Registration', a.encounter_datetime, null)) AS registration_date,
                max(if(b.name='HIV Enrollment', a.encounter_datetime, null)) AS hiv_enrolled_date,
                max(if(b.name='TB Enrollment', a.encounter_datetime, null)) AS tb_enrolled_date,
                max(if(b.name='MCH Child Enrollment', a.encounter_datetime, null)) AS mch_child_enrolled_date,
                max(if(b.name='MCH Mother Enrollment', a.encounter_datetime, null)) AS mch_mother_enrolled_date
            FROM encounter a
            LEFT JOIN encounter_type b ON b.encounter_type_id = a.encounter_type
            WHERE a.voided = 0 AND b.retired = 0
            GROUP BY a.patient_id
            ORDER BY a.patient_id, a.encounter_datetime
        ) h ON h.patient_id = b.patient_id
        LEFT JOIN (
            SELECT
                a.person_id,
                lower(b.name) AS cause_of_death
            FROM person a
            INNER JOIN concept_name b ON b.concept_id = a.cause_of_death AND b.concept_name_type = 'FULLY_SPECIFIED' AND b.locale = 'en' AND b.voided = 0
        ) i ON i.person_id = b.patient_id
        WHERE a.voided = 0 AND b.voided = 0";

        $host = \Config::get('database.connections.pl_mysql.host');
        $database = $this->emr_backup_file->database_name;
        $username = \Config::get('database.connections.pl_mysql.username');
        $password = \Config::get('database.connections.pl_mysql.password');

        $connection = new \PDO("mysql:host=$host;dbname=$database", $username,$password, [ \PDO::ATTR_PERSISTENT => true ]);
        $statement = $connection->prepare($query_mysql, [ \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY ]);

        if ($statement->execute()) {
            while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
                \App\Models\Patient::where('facility_id', $this->emr_backup_file->facility_id)
                                        ->where('emr_patient_id', $row['emr_patient_id'])
                                        ->update($row);
            }
        }
 
        \App\Jobs\GetPatientVisitsKenyaEMR::dispatch($this->emr_backup_file)->onQueue('get_patient_visits');
    }
}