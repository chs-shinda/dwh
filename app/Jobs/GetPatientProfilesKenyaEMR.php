<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GetPatientProfilesKenyaEMR implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emr_backup_file;

    public function __construct(\App\Models\EmrBackupFile $emr_backup_file)
    {
        $this->emr_backup_file = $emr_backup_file;
    }

    public function handle()
    {
        $query_mysql = "SELECT
            ? AS facility_id,
            a.patient_id AS emr_patient_id,
            CASE
                WHEN a.Gender = 'M' THEN 'male'
                WHEN a.Gender = 'F' THEN 'female'
                ELSE 'other'
            END AS sex,
            a.DOB AS dob,
            null AS dob_estimated,
            a.dead,
            CASE
                WHEN a.death_date IS NULL THEN a.death_date
                ELSE DATE_FORMAT(a.death_date,'%Y-%m-%d')
            END AS dod,
            lower(a.marital_status) AS marital_status,
            lower(a.education_level) AS education_level,
            CASE
                WHEN b.entry_point = 5622 THEN 'other non-coded'
                WHEN b.entry_point = 159937 THEN 'maternal and child health program'
                WHEN b.entry_point = 159938 THEN 'home based hiv testing program'
                WHEN b.entry_point = 160536 THEN 'adult inpatient service'
                WHEN b.entry_point = 160537 THEN 'pediatric inpatient service'
                WHEN b.entry_point = 160538 THEN 'prevention of maternal to child transmission program'
                WHEN b.entry_point = 160539 THEN 'voluntary counseling and testing program'
                WHEN b.entry_point = 160541 THEN 'tuberculosis treatment program'
                WHEN b.entry_point = 160542 THEN 'outpatient department'
                WHEN b.entry_point = 160544 THEN 'under five clinic'
                WHEN b.entry_point = 160563 THEN 'transfer in'
                WHEN b.entry_point = 162050 THEN 'comprehensive care center'
                ELSE null
            END AS entry_point,
            CASE
                WHEN b.date_confirmed_hiv_positive IS NULL THEN b.date_confirmed_hiv_positive
                ELSE DATE_FORMAT(b.date_confirmed_hiv_positive,'%Y-%m-%d')
            END AS hiv_positive_confirmed_date,
            lower(b.hiv_positive_confirmed_facility) AS hiv_positive_confirmed_facility,
            CASE
                WHEN b.transfer_in_date IS NULL THEN b.transfer_in_date
                ELSE DATE_FORMAT(b.transfer_in_date,'%Y-%m-%d')
            END AS transfer_in_date,
            lower(b.transfer_in_facility) AS transfer_in_facility,
            lower(b.transfer_in_district) AS transfer_in_district,
            CASE
                WHEN b.date_started_art_at_transferring_facility IS NULL THEN b.date_started_art_at_transferring_facility
                ELSE DATE_FORMAT(b.date_started_art_at_transferring_facility,'%Y-%m-%d')
            END AS transfer_in_art_start_date,
            lower(a.unique_patient_no) AS ccc,
            CASE
                WHEN b.date_first_enrolled_in_care IS NULL THEN b.date_first_enrolled_in_care
                ELSE DATE_FORMAT(b.date_first_enrolled_in_care,'%Y-%m-%d')
            END AS hiv_enrolled_date,
            null AS mch_child_enrolled_date,
            null AS mch_mother_enrolled_date,
            null AS registration_date,
            null AS tb_enrolled_date,
            null AS cause_of_death
        FROM etl_patient_demographics a
        LEFT JOIN (
              SELECT
                    a.patient_id,
                    min(a.entry_point) as entry_point,
                    min(a.date_confirmed_hiv_positive) as date_confirmed_hiv_positive,
                    min(a.facility_confirmed_hiv_positive) AS hiv_positive_confirmed_facility,
                    max(a.transfer_in_date) as transfer_in_date,
                    max(a.facility_transferred_from) AS transfer_in_facility,
                    max(a.district_transferred_from) AS transfer_in_district,
                    min(a.date_started_art_at_transferring_facility) as date_started_art_at_transferring_facility,
                    min(a.date_first_enrolled_in_care) as date_first_enrolled_in_care
              FROM etl_hiv_enrollment a
              WHERE a.voided = 0
              GROUP BY a.patient_id
        ) b ON a.patient_id = b.patient_id
        WHERE a.voided = 0
        ORDER BY a.patient_id";

        $host = \Config::get('database.connections.pl_mysql.host');
        $database = $this->emr_backup_file->database_name.'_etl';
        $username = \Config::get('database.connections.pl_mysql.username');
        $password = \Config::get('database.connections.pl_mysql.password');

        $connection = new \PDO("mysql:host=$host;dbname=$database", $username,$password, [ \PDO::ATTR_PERSISTENT => true ]);
        $statement = $connection->prepare($query_mysql, [ \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY ]);

        if ($statement->execute([$this->emr_backup_file->facility_id])) {
            while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
                $row['hiv_positive_confirmed_facility'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['hiv_positive_confirmed_facility'])), "UTF-8");
                $row['transfer_in_facility'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['transfer_in_facility'])), "UTF-8");
                $row['transfer_in_district'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['transfer_in_district'])), "UTF-8");
                $row['ccc'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['ccc'])), "UTF-8");
                \App\Models\Patient::insert($row);
            }
        }
 
        \App\Jobs\GetMissingPatientProfileVariablesKenyaEMR::dispatch($this->emr_backup_file)->onQueue('get_patient_profiles');
    }
}