<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GetPatientProfilesOpenMRS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emr_backup_file;

    public function __construct(\App\Models\EmrBackupFile $emr_backup_file)
    {
        $this->emr_backup_file = $emr_backup_file;
    }

    public function handle()
    {
        $query_mysql = "SELECT
            ? AS facility_id,
            a.person_id AS emr_patient_id,
            CASE WHEN a.gender = 'M' THEN 'male' WHEN a.gender = 'F' THEN 'female' ELSE 'other' END AS sex,
            CASE WHEN a.birthdate IS NULL THEN a.birthdate ELSE DATE_FORMAT(a.birthdate,'%Y-%m-%d') END AS dob,
            a.birthdate_estimated AS dob_estimated,
            a.dead,
            CASE WHEN a.death_date IS NULL THEN a.death_date ELSE DATE_FORMAT(a.death_date,'%Y-%m-%d') END AS dod,
            c.marital_status,
            c.education_level,
            c.entry_point,
            CASE WHEN d.hiv_positive_confirmed_date IS NULL THEN d.hiv_positive_confirmed_date ELSE DATE_FORMAT(d.hiv_positive_confirmed_date,'%Y-%m-%d') END AS hiv_positive_confirmed_date,
            LEFT(d.hiv_positive_confirmed_facility, 191) AS hiv_positive_confirmed_facility,
            CASE WHEN d.transfer_in_date IS NULL THEN d.transfer_in_date ELSE DATE_FORMAT(d.transfer_in_date,'%Y-%m-%d') END AS transfer_in_date,
            d.transferring_facility as transfer_in_facility,
            d.transferring_facility_district as transfer_in_district,
            CASE WHEN d.transferring_facility_art_start_date IS NULL THEN d.transferring_facility_art_start_date ELSE DATE_FORMAT(d.transferring_facility_art_start_date,'%Y-%m-%d') END AS transfer_in_art_start_date,
            -- e.district_reg_number,
            -- e.hei_id,
            -- e.national_id,
            -- e.patient_clinic_number,
            -- e.tb_treatment_number,
            e.upn AS ccc,
            CASE WHEN d.hiv_enrolled_date IS NULL THEN CASE WHEN h.hiv_enrolled_date IS NULL THEN h.hiv_enrolled_date ELSE DATE_FORMAT(h.hiv_enrolled_date,'%Y-%m-%d') END ELSE DATE_FORMAT(d.hiv_enrolled_date,'%Y-%m-%d') END AS hiv_enrolled_date,
            CASE WHEN h.mch_child_enrolled_date IS NULL THEN h.mch_child_enrolled_date ELSE DATE_FORMAT(h.mch_child_enrolled_date,'%Y-%m-%d') END AS mch_child_enrolled_date,
            CASE WHEN h.mch_mother_enrolled_date IS NULL THEN h.mch_mother_enrolled_date ELSE DATE_FORMAT(h.mch_mother_enrolled_date,'%Y-%m-%d') END AS mch_mother_enrolled_date,
            CASE WHEN h.registration_date IS NULL THEN h.registration_date ELSE DATE_FORMAT(h.registration_date,'%Y-%m-%d') END AS registration_date,
            CASE WHEN h.tb_enrolled_date IS NULL THEN h.tb_enrolled_date ELSE DATE_FORMAT(h.tb_enrolled_date,'%Y-%m-%d') END AS tb_enrolled_date,
            i.cause_of_death
        FROM person a
        INNER JOIN patient b ON b.patient_id = a.person_id
        LEFT JOIN (
            SELECT
                a.person_id AS patient_id,
                max(if(a.concept_id=1054,lower(b.name),null)) AS marital_status,
                max(if(a.concept_id=1368,lower(b.name),null)) AS education_level,
                null AS entry_point
            FROM obs a
            LEFT JOIN concept_name b ON b.concept_id = a.value_coded AND b.concept_name_type = 'FULLY_SPECIFIED' AND b.locale = 'en' AND b.voided = 0
            WHERE a.concept_id IN (1054,1368) AND a.voided = 0
            GROUP BY person_id
        ) c ON c.patient_id = b.patient_id
        LEFT JOIN ( 
            SELECT  
                a.person_id AS patient_id,
                max(if(a.concept_id=1672,a.value_datetime,null)) AS transfer_in_date,
                max(if(a.concept_id=1673,lower(a.value_text),null)) AS transferring_facility,
                max(if(a.concept_id=6161,lower(a.value_text),null)) AS transferring_facility_district,
                null AS transferring_facility_art_start_date,
                max(if(a.concept_id=6740,a.value_datetime,null)) AS hiv_positive_confirmed_date,
                max(if(a.concept_id=6741,lower(a.value_text),null)) AS hiv_positive_confirmed_facility,
                max(if(a.concept_id=6742,a.value_datetime,null)) AS hiv_enrolled_date,
                null AS previous_arv_status
            FROM obs a
            WHERE a.concept_id IN (1672, 1673, 6161, 6740, 6741, 6742) AND a.voided = 0
            GROUP BY person_id
        ) d ON d.patient_id = b.patient_id
        LEFT JOIN (
            SELECT
                a.patient_id,
                max(if(b.uuid='e2871922-024c-48a7-9803-854b97da1c2a',lower(a.identifier),null)) AS upn,
                null AS district_reg_number,
                max(if(b.uuid='aa928c86-ee4b-4f76-b77a-02e2d63cd660',lower(a.identifier),null)) AS tb_treatment_number,
                null AS patient_clinic_number,
                null AS national_id,
                max(if(b.uuid='ff3ddecf-ab5a-102d-be97-85aedb3d9f67',lower(a.identifier),null)) AS hei_id
            FROM patient_identifier a
            JOIN patient_identifier_type b ON a.identifier_type=b.patient_identifier_type_id
            WHERE voided = 0
            GROUP BY a.patient_id  
        ) e ON e.patient_id = b.patient_id
        LEFT JOIN (
            SELECT
                patient_id,
                max(encounter_datetime) AS last_visit_date,
                min(encounter_datetime) AS first_visit_date
            FROM encounter a
            WHERE a.voided = 0
            GROUP BY a.patient_id
            ORDER BY a.patient_id, encounter_datetime
        ) g ON g.patient_id = b.patient_id
        LEFT JOIN (
            SELECT
                patient_id,
                max(if(b.name='ADULTINITIAL' OR b.name='PEDSINITIAL', a.encounter_datetime, null)) AS registration_date,
                max(if(b.name='PSC INITIAL', a.encounter_datetime, null)) AS hiv_enrolled_date,
                null AS tb_enrolled_date,
                null AS mch_child_enrolled_date,
                null AS mch_mother_enrolled_date
            FROM encounter a
            LEFT JOIN encounter_type b ON b.encounter_type_id = a.encounter_type
            WHERE a.voided = 0 AND b.retired = 0
            GROUP BY a.patient_id
            ORDER BY a.patient_id, a.encounter_datetime
        ) h ON h.patient_id = b.patient_id
        LEFT JOIN (
            SELECT
                a.person_id,
                lower(b.name) AS cause_of_death
            FROM person a
            INNER JOIN concept_name b ON b.concept_id = a.cause_of_death AND b.concept_name_type = 'FULLY_SPECIFIED' AND b.locale = 'en' AND b.voided = 0
        ) i ON i.person_id = b.patient_id
        WHERE a.voided = 0 and b.voided = 0 AND b.patient_id IN (
            SELECT
                patient_id
            FROM encounter WHERE encounter_type IN (
                SELECT encounter_type_id FROM encounter_type WHERE encounter_type.name IN ('GREEN CARD','PSC INITIAL', 'PSC RETURN')
            )
        )";

        $host = \Config::get('database.connections.pl_mysql.host');
        $database = $this->emr_backup_file->database_name;
        $username = \Config::get('database.connections.pl_mysql.username');
        $password = \Config::get('database.connections.pl_mysql.password');

        $connection = new \PDO("mysql:host=$host;dbname=$database", $username,$password, [ \PDO::ATTR_PERSISTENT => true ]);
        $statement = $connection->prepare($query_mysql, [ \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY ]);

        if ($statement->execute([$this->emr_backup_file->facility_id])) {
            while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
                $row['hiv_positive_confirmed_facility'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['hiv_positive_confirmed_facility'])), "UTF-8");
                $row['transfer_in_facility'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['transfer_in_facility'])), "UTF-8");
                $row['transfer_in_district'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['transfer_in_district'])), "UTF-8");
                $row['ccc'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['ccc'])), "UTF-8");
                \App\Models\Patient::insert($row);
            }
        }

        \App\Jobs\GetPatientVisitsOpenMRS::dispatch($this->emr_backup_file)->onQueue('get_patient_visits');
    }
}