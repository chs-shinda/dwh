<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GetPatientSummariesKenyaEMR implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emr_backup_file;

    public function __construct(\App\Models\EmrBackupFile $emr_backup_file)
    {
        $this->emr_backup_file = $emr_backup_file;
    }

    public function handle()
    {
        $query_pgsql = "SELECT
            a.id,
            a.facility_id,
            a.emr_patient_id,
            b.regimen as initial_regimen,
            b.regimen_name as initial_regimen_name,
            b.regimen_line as initial_regimen_line,
            b.visit_date as initial_regimen_date,
            b.visit_date as art_start_date,
            c.regimen as current_regimen,
            c.regimen_name as current_regimen_name,
            c.regimen_line as current_regimen_line,
            c.visit_date as current_regimen_date,
            d.visit_date as first_visit_date,
            e.visit_date as last_visit_date,
            f.next_appointment_date,
            g.who_stage as initial_who_stage,
            g.visit_date as initial_who_stage_date,
            h.who_stage as current_who_stage,
            h.visit_date as current_who_stage_date,
            i.cd4 as initial_cd4,
            i.visit_date as initial_cd4_date,
            j.cd4_percent as initial_cd4_percent,
            j.visit_date as initial_cd4_percent_date,
            k.cd4 as current_cd4,
            k.visit_date as current_cd4_date,
            l.cd4_percent as current_cd4_percent,
            l.visit_date as current_cd4_percent_date,
            m.vl as initial_vl,
            m.visit_date as initial_vl_date,
            n.vl as current_vl,
            n.visit_date as current_vl_date
        from patients a
        left join (
            select
                facility_id,
                emr_patient_id,
                visit_date,
                regimen,
                regimen_name,
                regimen_line,
                row_number() over (partition by facility_id, emr_patient_id order by visit_date asc) r
                from visits
                where regimen is not null
        ) b on b.facility_id = a.facility_id and b.emr_patient_id = a.emr_patient_id and b.r=1
        left join (
            select
                facility_id,
                emr_patient_id,
                visit_date,
                regimen,
                regimen_name,
                regimen_line,
                row_number() over (partition by facility_id, emr_patient_id order by visit_date desc) r
                from visits
                where regimen is not null
        ) c on c.facility_id = a.facility_id and c.emr_patient_id = a.emr_patient_id and c.r=1
        left join (
            select
                facility_id,
                emr_patient_id,
                visit_date,
                row_number() over (partition by facility_id, emr_patient_id order by visit_date asc) r
                from visits
                where visit_date is not null
        ) d on d.facility_id = a.facility_id and d.emr_patient_id = a.emr_patient_id and d.r=1
        left join (
            select
                facility_id,
                emr_patient_id,
                visit_date,
                row_number() over (partition by facility_id, emr_patient_id order by visit_date desc) r
                from visits
                where visit_date is not null
        ) e on e.facility_id = a.facility_id and e.emr_patient_id = a.emr_patient_id and e.r=1
        left join (
            select
                facility_id,
                emr_patient_id,
                next_appointment_date,
                row_number() over (partition by facility_id, emr_patient_id order by visit_date desc) r
                from visits
                where visit_date is not null
        ) f on f.facility_id = a.facility_id and f.emr_patient_id = a.emr_patient_id and f.r=1
        left join (
            select
                facility_id,
                emr_patient_id,
                visit_date,
                who_stage,
                row_number() over (partition by facility_id, emr_patient_id order by visit_date asc) r
                from visits
                where who_stage is not null
        ) g on g.facility_id = a.facility_id and g.emr_patient_id = a.emr_patient_id and g.r=1
        left join (
            select
                facility_id,
                emr_patient_id,
                visit_date,
                who_stage,
                row_number() over (partition by facility_id, emr_patient_id order by visit_date desc) r
                from visits
                where who_stage is not null
        ) h on h.facility_id = a.facility_id and h.emr_patient_id = a.emr_patient_id and h.r=1
        left join (
            select
                facility_id,
                emr_patient_id,
                visit_date,
                cd4,
                row_number() over (partition by facility_id, emr_patient_id order by visit_date asc) r
                from visits
                where cd4 is not null
        ) i on i.facility_id = a.facility_id and i.emr_patient_id = a.emr_patient_id and i.r=1
        left join (
            select
                facility_id,
                emr_patient_id,
                visit_date,
                cd4_percent,
                row_number() over (partition by facility_id, emr_patient_id order by visit_date asc) r
                from visits
                where cd4_percent is not null
        ) j on j.facility_id = a.facility_id and j.emr_patient_id = a.emr_patient_id and j.r=1
        left join (
            select
                facility_id,
                emr_patient_id,
                visit_date,
                cd4,
                row_number() over (partition by facility_id, emr_patient_id order by visit_date desc) r
                from visits
                where cd4 is not null
        ) k on k.facility_id = a.facility_id and k.emr_patient_id = a.emr_patient_id and k.r=1
        left join (
            select
                facility_id,
                emr_patient_id,
                visit_date,
                cd4_percent,
                row_number() over (partition by facility_id, emr_patient_id order by visit_date desc) r
                from visits
                where cd4_percent is not null
        ) l on l.facility_id = a.facility_id and l.emr_patient_id = a.emr_patient_id and l.r=1
        left join (
            select
                facility_id,
                emr_patient_id,
                visit_date,
                viral_load as vl,
                row_number() over (partition by facility_id, emr_patient_id order by visit_date asc) r
                from visits
                where viral_load is not null
        ) m on m.facility_id = a.facility_id and m.emr_patient_id = a.emr_patient_id and m.r=1
        left join (
            select
                facility_id,
                emr_patient_id,
                visit_date,
                viral_load as vl,
                row_number() over (partition by facility_id, emr_patient_id order by visit_date desc) r
                from visits
                where viral_load is not null
        ) n on n.facility_id = a.facility_id and n.emr_patient_id = a.emr_patient_id and n.r=1
        where a.facility_id = ?";

        $host = \Config::get('database.connections.pgsql.host');
        $database = \Config::get('database.connections.pgsql.database');
        $username = \Config::get('database.connections.pgsql.username');
        $password = \Config::get('database.connections.pgsql.password');

        $connection = new \PDO("pgsql:host=$host;dbname=$database", $username,$password);
        $statement = $connection->prepare($query_pgsql, [ \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY ]); 
        if($statement->execute([$this->emr_backup_file->facility_id])) {
            while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
                \App\Models\Patient::where('id', $row['id'])->update($row);
            }
        }

        if($this->emr_backup_file->emr_type->name == 'KenyaEMR') {
            \App\Jobs\GetCurrentKenyaEMR::dispatch($this->emr_backup_file);
        }
        elseif($this->emr_backup_file->emr_type->name == 'OpenMRS') {
            \App\Jobs\GetCurrentOpenMRS::dispatch($this->emr_backup_file);
        }
    }
}
