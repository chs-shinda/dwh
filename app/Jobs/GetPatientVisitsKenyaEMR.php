<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GetPatientVisitsKenyaEMR implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emr_backup_file;

    public function __construct(\App\Models\EmrBackupFile $emr_backup_file)
    {
        $this->emr_backup_file = $emr_backup_file;
    }

    public function handle()
    {
        $query_mysql = "SELECT
            ? as facility_id,
            null AS patient_id,
            a.emr_patient_id,
            a.emr_visit_id,
            a.visit_date,
            a.visit_type,
            a.encounter_type,
            a.weight,
            a.height,
            a.systolic_pressure,
            a.diastolic_pressure,
            a.temperature,
            a.pulse_rate,
            a.respiratory_rate,
            a.oxygen_saturation,
            a.muac,
            a.nutritional_status,
            a.population_type,
            a.key_population_type,
            a.who_stage,
            a.presenting_complaints,
            a.clinical_notes,
            a.on_anti_tb_drugs,
            a.on_ipt,
            a.ever_on_ipt,
            a.spatum_smear_ordered,
            a.chest_xray_ordered,
            a.genexpert_ordered,
            a.spatum_smear_result,
            a.chest_xray_result,
            a.genexpert_result,
            a.referral,
            a.clinical_tb_diagnosis,
            a.contact_invitation,
            a.evaluated_for_ipt,
            a.has_known_allergies,
            a.has_chronic_illnesses_cormobidities,
            a.has_adverse_drug_reaction,
            a.pregnancy_status,
            a.wants_pregnancy,
            a.pregnancy_outcome,
            a.anc_number,
            a.expected_delivery_date,
            a.last_menstrual_period,
            a.gravida,
            a.parity,
            a.full_term_pregnancies,
            a.abortion_miscarriages,
            a.family_planning_status,
            a.family_planning_method,
            a.reason_not_using_family_planning,        
            b.cough_for_2wks_or_more,
            b.confirmed_tb_contact,
            b.fever_for_2wks_or_more,
            b.noticeable_weight_loss,
            b.night_sweat_for_2wks_or_more,
            a.tb_status,
            a.tb_treatment_no,
            a.ctx_adherence,
            a.ctx_dispensed,
            a.dapsone_adherence,
            a.dapsone_dispensed,
            a.inh_dispensed,
            c.regimen,
            c.regimen_name,
            c.regimen_line,
            c.regimen_start_date,
            a.arv_adherence,
            a.poor_arv_adherence_reason,
            a.poor_arv_adherence_reason_other,
            a.pwp_disclosure,
            a.pwp_partner_tested,
            a.condom_provided,
            a.screened_for_sti,
            a.cacx_screening,
            e.cd4_count as cd4,
            f.cd4_percent,
            case when d.viral_load = 'LDL' then 0 else d.viral_load end as viral_load,
            case when d.viral_load = 'LDL' then 'ldl' else null end as viral_load_ldl,
            a.sti_partner_notification,
            a.at_risk_population,
            a.next_appointment_date,
            a.next_appointment_reason,
            a.differentiated_care,
            g.transfer_out_facility,
            g.transfer_out_date
        from (
            select
                a.patient_id as emr_patient_id,
                max(a.visit_id) as emr_visit_id,
                max(a.visit_date) as visit_date,
                CASE WHEN max(a.visit_scheduled) = 'visit_scheduled' THEN 'scheduled' ELSE null END AS visit_type,
                lower(max(a.person_present)) as encounter_type,
                max(a.weight) as weight,
                max(a.height) as height,
                max(a.systolic_pressure) as systolic_pressure,
                max(a.diastolic_pressure) as diastolic_pressure,
                max(a.temperature) as temperature,
                max(a.pulse_rate) as pulse_rate,
                max(a.respiratory_rate) as respiratory_rate,
                max(a.oxygen_saturation) as oxygen_saturation,
                max(a.muac) as muac,
                lower(max(a.nutritional_status)) as nutritional_status,
                lower(max(a.population_type)) as population_type,
                lower(max(key_population_type)) as key_population_type,
                lower(max(a.who_stage)) as who_stage,
                lower(max(a.presenting_complaints)) as presenting_complaints,
                lower(max(a.clinical_notes)) as clinical_notes,
                lower(max(on_anti_tb_drugs)) as on_anti_tb_drugs,
                lower(max(on_ipt)) as on_ipt,
                lower(max(ever_on_ipt)) as ever_on_ipt,
                lower(max(spatum_smear_ordered)) as spatum_smear_ordered,
                lower(max(chest_xray_ordered)) as chest_xray_ordered,
                lower(max(genexpert_ordered)) as genexpert_ordered,
                lower(max(spatum_smear_result)) as spatum_smear_result,
                lower(max(chest_xray_result)) as chest_xray_result,
                lower(max(genexpert_result)) as genexpert_result,
                lower(max(referral)) as referral,
                lower(max(clinical_tb_diagnosis)) as clinical_tb_diagnosis,
                lower(max(contact_invitation)) as contact_invitation,
                lower(max(evaluated_for_ipt)) as evaluated_for_ipt,
                lower(max(has_known_allergies)) as has_known_allergies,
                lower(max(has_chronic_illnesses_cormobidities)) as has_chronic_illnesses_cormobidities,
                lower(max(has_adverse_drug_reaction)) as has_adverse_drug_reaction,
                lower(max(pregnancy_status)) as pregnancy_status,
                lower(max(wants_pregnancy)) as wants_pregnancy,
                lower(max(pregnancy_outcome)) as pregnancy_outcome,
                max(anc_number) as anc_number,
                max(expected_delivery_date) as expected_delivery_date,
                max(last_menstrual_period) as last_menstrual_period,
                max(gravida) as gravida,
                max(parity) as parity,
                max(full_term_pregnancies) as full_term_pregnancies,
                max(abortion_miscarriages) as abortion_miscarriages,
                lower(max(family_planning_status)) as family_planning_status,
                lower(max(family_planning_method)) as family_planning_method,
                lower(max(reason_not_using_family_planning)) as reason_not_using_family_planning,
                lower(max(tb_status)) as tb_status,
                lower(max(tb_treatment_no)) as tb_treatment_no,
                lower(max(ctx_adherence)) as ctx_adherence,
                lower(max(ctx_dispensed)) as ctx_dispensed,
                lower(max(dapsone_adherence)) as dapsone_adherence,
                lower(max(dapsone_dispensed)) as dapsone_dispensed,
                lower(max(inh_dispensed)) as inh_dispensed,
                lower(max(arv_adherence)) as arv_adherence,
                lower(max(poor_arv_adherence_reason)) as poor_arv_adherence_reason,
                lower(max(poor_arv_adherence_reason_other)) as poor_arv_adherence_reason_other,
                lower(max(pwp_disclosure)) as pwp_disclosure,
                lower(max(pwp_partner_tested)) as pwp_partner_tested,
                lower(max(condom_provided)) as condom_provided,
                lower(max(screened_for_sti)) as screened_for_sti,
                lower(max(cacx_screening)) as cacx_screening,
                lower(max(sti_partner_notification)) as sti_partner_notification,
                lower(max(at_risk_population)) as at_risk_population,
                lower(max(system_review_finding)) as system_review_finding,
                max(next_appointment_date) as next_appointment_date,
                lower(max(next_appointment_reason)) as next_appointment_reason,
                lower(max(differentiated_care)) as differentiated_care
            from hiv_followup a
            where a.patient_id is not null and a.visit_date is not null
            group by a.patient_id, a.visit_date
        ) a
        left join (
            select
                a.patient_id as emr_patient_id,
                max(a.visit_id) as emr_visit_id,
                max(a.visit_date) as visit_date,
                max(a.cough_for_2wks_or_more) as cough_for_2wks_or_more,
                max(a.confirmed_tb_contact) as confirmed_tb_contact,
                max(a.fever_for_2wks_or_more) as fever_for_2wks_or_more,
                max(a.noticeable_weight_loss) as noticeable_weight_loss,
                max(a.night_sweat_for_2wks_or_more) as night_sweat_for_2wks_or_more,
                max(a.resulting_tb_status) as resulting_tb_status,
                max(a.tb_treatment_start_date) as tb_treatment_start_date,
                max(a.notes) as notes
            from tb_screening a
            group by a.patient_id, a.visit_date
        ) b on a.emr_patient_id = b.emr_patient_id and a.visit_date = b.visit_date
        left outer join (
            select
                a.patient_id as emr_patient_id,
                a.date_started as regimen_start_date,
                a.regimen,
                a.regimen_name,
                a.regimen_line,
                a.date_discontinued as regimen_stop_date
            from drug_event a
        ) c ON c.emr_patient_id = a.emr_patient_id and c.regimen_start_date <= a.visit_date and (a.visit_date < c.regimen_stop_date or a.visit_date < ifnull(c.regimen_stop_date, now()))
        left join (
            select
                a.patient_id as emr_patient_id,
                max(a.visit_date) as visit_date,
                max(a.test_result) as viral_load
            from laboratory_extract a
            where lab_test  = 'HIV VIRAL LOAD'
            group by a.patient_id, a.visit_date
        ) d ON a.emr_patient_id = d.emr_patient_id and a.visit_date = d.visit_date
        left join (
            select
                a.patient_id as emr_patient_id,
                max(a.visit_date) as visit_date,
                max(a.test_result) as cd4_count
            from laboratory_extract a
            where lab_test  = 'CD4 Count'
            group by a.patient_id, a.visit_date
        ) e ON a.emr_patient_id = e.emr_patient_id and a.visit_date = e.visit_date
        left join (
            select
                a.patient_id as emr_patient_id,
                max(a.visit_date) as visit_date,
                max(a.test_result) as cd4_percent
            from laboratory_extract a
            where lab_test  = 'CD4 PERCENT '
            group by a.patient_id, a.visit_date
        ) f ON a.emr_patient_id = f.emr_patient_id and a.visit_date = f.visit_date
        left join (
            select
                a.patient_id as emr_patient_id,
                max(date_format(a.visit_date,'%Y-%m-%d')) as visit_date,
                max(a.transfer_facility) as transfer_out_facility,
                max(a.transfer_date) as transfer_out_date
            from patient_program_discontinuation a
            where a.discontinuation_reason =  'Transferred Out' and a.program_name = 'HIV'
            group by a.patient_id, date_format(a.visit_date,'%Y-%m-%d')
        ) g ON a.emr_patient_id = g.emr_patient_id and (a.visit_date = g.visit_date or a.visit_date = g.transfer_out_date)
        order by a.emr_patient_id, a.visit_date
        ";

        $host = \Config::get('database.connections.pl_mysql.host');
        $database = $this->emr_backup_file->database_name.'_dt';
        $username = \Config::get('database.connections.pl_mysql.username');
        $password = \Config::get('database.connections.pl_mysql.password');

        $connection = new \PDO("mysql:host=$host;dbname=$database", $username,$password);
        $statement = $connection->prepare($query_mysql, [ \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY ]); 
        if($statement->execute([$this->emr_backup_file->facility_id])) {
            while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
                $row['presenting_complaints'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['presenting_complaints'])), "UTF-8");
                $row['clinical_notes'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['clinical_notes'])), "UTF-8");
                $row['anc_number'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['anc_number'])), "UTF-8");
                $row['regimen'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['regimen'])), "UTF-8");
                $row['regimen_name'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['regimen_name'])), "UTF-8");
                $row['regimen_line'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['regimen_line'])), "UTF-8");
                $row['poor_arv_adherence_reason_other'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['poor_arv_adherence_reason_other'])), "UTF-8");
                $row['viral_load_ldl'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['viral_load_ldl'])), "UTF-8");
                \App\Models\Visit::insert($row);
            }
        }

        $visit = \App\Models\Visit::where('facility_id', $this->emr_backup_file->facility_id)->orderBy('visit_date', 'DESC')->first();

        $this->emr_backup_file->date_processed = \Carbon\Carbon::now();
        $this->emr_backup_file->process_time = \Carbon\Carbon::now()->timestamp - \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->emr_backup_file->date_restored)->timestamp;
        $this->emr_backup_file->last_visit_date = isset($visit->visit_date) ? $visit->visit_date:null;
        $this->emr_backup_file->save();
        
        if(\File::exists($this->emr_backup_file->backup_file) && (\File::mimeType($this->emr_backup_file->backup_file) == 'application/x-gzip' || \File::mimeType($this->emr_backup_file->backup_file) == 'application/zip')) {
            if(\File::exists(storage_path().'/app/uploads/'.$this->emr_backup_file->database_name.'.sql')) {
                \File::delete(storage_path().'/app/uploads/'.$this->emr_backup_file->database_name.'.sql');
            }
        }

        \App\Jobs\GetPatientSummariesKenyaEMR::dispatch($this->emr_backup_file)->onQueue('get_patient_summaries');
    }
}
