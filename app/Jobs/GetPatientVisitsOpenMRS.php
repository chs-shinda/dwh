<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GetPatientVisitsOpenMRS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emr_backup_file;

    public function __construct(\App\Models\EmrBackupFile $emr_backup_file)
    {
        $this->emr_backup_file = $emr_backup_file;
    }

    public function handle()
    {
        $query_mysql = "SELECT
            ? as facility_id,
            null AS patient_id,
            e.patient_id as emr_patient_id,
            e.visit_id as emr_visit_id,
            CASE WHEN e.encounter_datetime IS NULL THEN NULL ELSE DATE_FORMAT(e.encounter_datetime,'%Y-%m-%d') END AS visit_date,
            lower(max(if(o.concept_id=1501 AND o.value_coded = 1493,'scheduled visit', if(o.concept_id=1501 AND o.value_coded = 1494,'unscheduled visit', null)))) as visit_type,
            null as encounter_type,
            max(if(o.concept_id=5089,o.value_numeric,null)) as weight,
            max(if(o.concept_id=5090,o.value_numeric,null)) as height,
            max(if(o.concept_id=5085,o.value_numeric,null)) as systolic_pressure,
            max(if(o.concept_id=5086,o.value_numeric,null)) as diastolic_pressure,
            max(if(o.concept_id=5088,o.value_numeric,null)) as temperature,
            max(if(o.concept_id=5087,o.value_numeric,null)) as pulse_rate,
            max(if(o.concept_id=5242,o.value_numeric,null)) as respiratory_rate,
            null as oxygen_saturation,
            null as muac,
            lower(max(if(o.concept_id=7797,cn.name,null))) as nutritional_status, 
            lower(max(if(o.concept_id=7766, cn.name, null))) as population_type, 
            lower(max(if(o.concept_id=7766, cn.name,null))) as key_population_type, 
            lower(max(if(o.concept_id=6144 or o.concept_id=6377,cn.name,null))) as who_stage,
            lower(max(if(o.concept_id=1154,cn.name,null))) as presenting_complaints,
            lower(max(if(o.concept_id=7456,o.value_text,null))) as clinical_notes,
            lower(max(if(o.concept_id=1573 AND o.value_coded = 1500, 'Yes', 'No'))) as on_anti_tb_drugs,
            lower(max(if(o.concept_id=1573 AND o.value_coded = 656, 'Yes', 'No')))as on_ipt,
            lower(max(if(o.concept_id=6884, 'Yes', 'No'))) as ever_on_ipt,
            lower(max(if(o.concept_id=1271 AND o.value_coded = 307,'Yes','No'))) as spatum_smear_ordered,
            lower(max(if(o.concept_id=1271 AND o.value_coded = 12,'Yes','No'))) as chest_xray_ordered,
            null as genexpert_ordered,
            lower(max(if(o.concept_id=307,cn.name,null))) as spatum_smear_result,
            lower(max(if(o.concept_id=12,cn.name,null))) as chest_xray_result,
            null as genexpert_result,
            lower(max(if(o.concept_id=1272,cn.name,null))) as referral,
            null as clinical_tb_diagnosis,
            lower(max(if(o.concept_id=6896,cn.name,null))) as contact_invitation,
            lower(max(if(o.concept_id=6897,cn.name,null))) as evaluated_for_ipt,
            lower(max(if(o.concept_id=1433,cn.name,null))) as has_known_allergies,
            lower(max(if(o.concept_id=7399,'Yes', 'No'))) as has_chronic_illnesses_cormobidities,
            lower(max(if(o.concept_id=6893,cn.name,null))) as has_adverse_drug_reaction,
            lower(max(if(o.concept_id=5272,cn.name,null))) as pregnancy_status,
            lower(max(if(o.concept_id=1443,cn.name,null))) as wants_pregnancy,
            null pregnancy_outcome, -- 48, 47, 46, 49, 50
            null as anc_number,
            CASE WHEN max(if(o.concept_id=5596,date(o.value_datetime),null)) IS NULL THEN NULL ELSE DATE_FORMAT(max(if(o.concept_id=5596,date(o.value_datetime),null)),'%Y-%m-%d') END AS expected_delivery_date,
            CASE WHEN max(if(o.concept_id=1427,date(o.value_datetime),null)) IS NULL THEN NULL ELSE DATE_FORMAT(max(if(o.concept_id=1427,date(o.value_datetime),null)),'%Y-%m-%d') END AS last_menstrual_period,
            max(if(o.concept_id=5624,o.value_numeric,null)) as gravida,
            max(if(o.concept_id=1053,o.value_numeric,null)) as parity,
            null as full_term_pregnancies,
            null as abortion_miscarriages,
            lower(max(if(o.concept_id=5271,cn.name,null))) as family_planning_status,
            lower(max(if(o.concept_id=374,cn.name,null))) as family_planning_method,
            lower(max(if(o.concept_id=6758,cn.name,null))) as reason_not_using_family_planning,
            lower(max(if(o.concept_id=6827,cn.name,null))) as cough_for_2wks_or_more,
            lower(max(if(o.concept_id=6513,cn.name,null))) as confirmed_tb_contact,
            lower(max(if(o.concept_id=6114,cn.name,null))) as chronic_cough,
            lower(max(if(o.concept_id=6826,cn.name,null))) as fever_for_2wks_or_more,
            lower(max(if(o.concept_id=6112,cn.name,null))) as noticeable_weight_loss,
            lower(max(if(o.concept_id=6511,cn.name,null))) as chest_pain,
            lower(max(if(o.concept_id=6828,cn.name,null))) as night_sweat_for_2wks_or_more,
            lower(max(if(o.concept_id=1659,cn.name,null))) as tb_status,
            lower(max(if(o.concept_id=6355,o.value_text,null))) as tb_treatment_no,
            lower(max(if(o.concept_id=6761,cn.name,null))) as ctx_adherence,
            lower(max(if(o.concept_id=6509 AND o.value_coded = 93, 'yes',null))) as ctx_dispensed,
            lower(max(if(o.concept_id=6761,cn.name,null))) as dapsone_adherence,
            lower(max(if(o.concept_id=6509 AND o.value_coded = 92, 'yes',null)))  as dapsone_dispensed,
            lower(max(if(o.concept_id=6785,cn.name,null))) as inh_dispensed,
            lower(max(if(o.concept_id=6760,cn.name,null))) as arv_adherence,
            null as poor_arv_adherence_reason,
            null as poor_arv_adherence_reason_other,
            lower(max(if(o.concept_id=1048,cn.name,null))) as pwp_disclosure,
            lower(max(if(o.concept_id=1363,cn.name,null))) as pwp_partner_tested,
            lower(max(if(o.concept_id=6781,cn.name,null))) as condom_provided,
            lower(max(if(o.concept_id=6780,cn.name,null))) as screened_for_sti,
            lower(max(if(o.concept_id=7826,cn.name,null))) as cacx_screening,
            max(if(o.concept_id=5497,o.value_numeric,null)) as cd4,
            max(if(o.concept_id=730,o.value_numeric,null)) as cd4_percent, 
            lower(max(if(o.concept_id=856,o.value_numeric,null))) as viral_load,
            lower(COALESCE(max(if(o.concept_id=856,o.value_numeric,null)), max(if(o.concept_id=1305 and o.value_coded = 1306, 0, null)))) as viral_load,
            lower(max(if(o.concept_id=1305 and o.value_coded = 1306, 'NOT DETECTED', null))) as viral_load_ldl,
            max(if(o.concept_id=1461,cn.name,null)) as regimen,
            max(if(o.concept_id=1461,cn.name,null)) as regimen_name,
            null as regimen_line,
            lower(max(if(o.concept_id=7827,cn.name,null))) as sti_partner_notification,
            lower(max(if(o.concept_id=6181,cn.name,null))) as at_risk_population,
            CASE WHEN max(if(o.concept_id=5096,o.value_datetime,null)) IS NULL THEN NULL ELSE DATE_FORMAT(max(if(o.concept_id=5096,o.value_datetime,null)),'%Y-%m-%d') END AS next_appointment_date,
            lower(max(if(o.concept_id=1939,cn.name,null))) as next_appointment_reason,
            null as differentiated_care
        FROM encounter e
        INNER JOIN (
            SELECT
                encounter_type_id,
                uuid,
                name
            FROM encounter_type WHERE name IN ('GREEN CARD','PSC INITIAL', 'PSC RETURN')
        ) et ON et.encounter_type_id=e.encounter_type
        LEFT OUTER JOIN obs o ON o.encounter_id=e.encounter_id AND o.concept_id IN (1501,1501,5089,5090,5085,5086,5088,5087,5242,7797,7766,7766,6144,6377,1154,7456,1573,1573,6884,1271,1271,307,12,1272,6896,6897,1433,7399,6893,5272,1443,5596,5596,1427,1427,5624,1053,5271,374,6758,6827,6513,6114,6826,6112,6511,6828,1659,6355,6761,6509,6761,6509,6785,6760,1048,1363,6781,6780,7826,5497,730,856,856,1305,1305,1461,7827,6181,5096,5096,1939)
        LEFT JOIN concept_name cn ON cn.concept_id = o.value_coded AND cn.concept_name_type = 'FULLY_SPECIFIED' AND cn.locale = 'en' AND cn.voided = 0
        WHERE e.voided=0
        GROUP BY e.patient_id, visit_date
        ORDER BY e.patient_id, visit_date";

        $host = \Config::get('database.connections.pl_mysql.host');
        $database = $this->emr_backup_file->database_name;
        $username = \Config::get('database.connections.pl_mysql.username');
        $password = \Config::get('database.connections.pl_mysql.password');

        $connection = new \PDO("mysql:host=$host;dbname=$database", $username,$password);
        $statement = $connection->prepare($query_mysql, [ \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY ]); 
        if($statement->execute([$this->emr_backup_file->facility_id])) {
            while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
                $row['presenting_complaints'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['presenting_complaints'])), "UTF-8");
                $row['clinical_notes'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['clinical_notes'])), "UTF-8");
                $row['anc_number'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['anc_number'])), "UTF-8");
                $row['regimen'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['regimen'])), "UTF-8");
                $row['regimen_name'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['regimen_name'])), "UTF-8");
                $row['regimen_line'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['regimen_line'])), "UTF-8");
                $row['poor_arv_adherence_reason_other'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['poor_arv_adherence_reason_other'])), "UTF-8");
                $row['viral_load_ldl'] = mb_convert_encoding(trim(preg_replace('/\s+/',' ', $row['viral_load_ldl'])), "UTF-8");
                \App\Models\Visit::insert($row);
            }
        }

        $visit = \App\Models\Visit::where('facility_id', $this->emr_backup_file->facility_id)->orderBy('visit_date', 'DESC')->first();

        $this->emr_backup_file->date_processed = \Carbon\Carbon::now();
        $this->emr_backup_file->process_time = \Carbon\Carbon::now()->timestamp - \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->emr_backup_file->date_restored)->timestamp;
        $this->emr_backup_file->last_visit_date = isset($visit->visit_date) ? $visit->visit_date:null;
        $this->emr_backup_file->save();
        
        if(\File::exists($this->emr_backup_file->backup_file) && (\File::mimeType($this->emr_backup_file->backup_file) == 'application/x-gzip' || \File::mimeType($this->emr_backup_file->backup_file) == 'application/zip')) {
            if(\File::exists(storage_path().'/app/uploads/'.$this->emr_backup_file->database_name.'.sql')) {
                \File::delete(storage_path().'/app/uploads/'.$this->emr_backup_file->database_name.'.sql');
            }
        }

        \App\Jobs\GetPatientSummaries::dispatch($this->emr_backup_file)->onQueue('get_patient_summaries');
    }
}