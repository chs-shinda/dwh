<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Symfony\Component\Process\Process;

class RestoreMysqlEmrBackupFile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emr_backup_file;

    public function __construct(\App\Models\EmrBackupFile $emr_backup_file)
    {
        $this->emr_backup_file = $emr_backup_file;
    }

    public function handle()
    {
        if(($this->emr_backup_file->emr_type->database_dialect == 'mysql') && ($this->emr_backup_file->backup_file) && ((\File::mimeType($this->emr_backup_file->backup_file) == 'application/x-gzip') || (\File::mimeType($this->emr_backup_file->backup_file) == 'application/zip') || (\File::mimeType($this->emr_backup_file->backup_file) == 'text/plain'))) {
            
            $facility_name = $this->emr_backup_file->facility->name;
            $facility_name = str_replace(' ', '', $facility_name);
            $facility_name = preg_replace('/[^A-Za-z0-9]/', '', $facility_name);
            $facility_name = strtolower($facility_name);
            $facility_name = substr($facility_name, 0, 10);
            $restore_start = \Carbon\Carbon::now()->timestamp;
            $database_name = $facility_name.'_'.$restore_start;            

            $command = sprintf('mysql --protocol=tcp --host=%s --port=%s --user=%s --password=%s -e "create database %s"',
                escapeshellarg(\Config::get('database.connections.pl_mysql.host')),
                escapeshellarg(\Config::get('database.connections.pl_mysql.port')),
                escapeshellarg(\Config::get('database.connections.pl_mysql.username')),
                escapeshellarg(\Config::get('database.connections.pl_mysql.password')),
                $database_name
            );

            $process = new Process($command);
            $process->setTimeout(null);
            $process->disableOutput();
            $process->mustRun();

            if($process->isSuccessful())
            {
                if(\File::mimeType($this->emr_backup_file->backup_file) == 'application/x-gzip') {
                    
                    $command2 = sprintf('gzip -dkc %s > %s',
                        $this->emr_backup_file->backup_file,
                        storage_path().'/app/uploads/'.$database_name.'.sql'
                    );

                    $process2 = new Process($command2);
                    $process2->setTimeout(null);
                    $process2->disableOutput();
                    $process2->mustRun();

                    if($process2->isSuccessful())
                    {
                        $command3 = sprintf('mysql --protocol=tcp --host=%s --port=%s --user=%s --password=%s --binary-mode --default-character-set=utf8 --comments --force %s < %s',
                            escapeshellarg(\Config::get('database.connections.pl_mysql.host')),
                            escapeshellarg(\Config::get('database.connections.pl_mysql.port')),
                            escapeshellarg(\Config::get('database.connections.pl_mysql.username')),
                            escapeshellarg(\Config::get('database.connections.pl_mysql.password')),
                            $database_name,
                            storage_path().'/app/uploads/'.$database_name.'.sql'
                        );

                        $process3 = new Process($command3);
                        $process3->setTimeout(null);
                        $process3->disableOutput();
                        $process3->mustRun();

                        if($process3->isSuccessful()) {

                            $this->emr_backup_file->date_restored = \Carbon\Carbon::now();
                            $this->emr_backup_file->restore_time = \Carbon\Carbon::now()->timestamp - $restore_start;
                            $this->emr_backup_file->database_name = $database_name;
                            $this->emr_backup_file->save();

                            if($this->emr_backup_file->emr_type->name == 'KenyaEMR') {
                                \App\Jobs\CreateKenyaEMRETL::dispatch($this->emr_backup_file)->onQueue('create_kenya_emr_etl');
                            } elseif ($this->emr_backup_file->emr_type->name == 'OpenMRS') {
                                \App\Jobs\DeletePreviousPatientVisits::dispatch($this->emr_backup_file)->onQueue('delete_previous_patient_visits');
                            }
                        }
                    }
                }
                elseif(\File::mimeType($this->emr_backup_file->backup_file) == 'text/plain') {

                    $command4 = sprintf('mysql --protocol=tcp --host=%s --port=%s --user=%s --password=%s --binary-mode --default-character-set=utf8 --comments --force %s < %s',
                        escapeshellarg(\Config::get('database.connections.pl_mysql.host')),
                        escapeshellarg(\Config::get('database.connections.pl_mysql.port')),
                        escapeshellarg(\Config::get('database.connections.pl_mysql.username')),
                        escapeshellarg(\Config::get('database.connections.pl_mysql.password')),
                        $database_name,
                        $this->emr_backup_file->backup_file
                    );

                    $process4 = new Process($command4);
                    $process4->setTimeout(null);
                    $process4->disableOutput();
                    $process4->mustRun();

                    if($process4->isSuccessful()) {

                        $this->emr_backup_file->date_restored = \Carbon\Carbon::now();
                        $this->emr_backup_file->restore_time = \Carbon\Carbon::now()->timestamp - $restore_start;
                        $this->emr_backup_file->database_name = $database_name;
                        $this->emr_backup_file->save();

                        if($this->emr_backup_file->emr_type->name == 'KenyaEMR') {
                            \App\Jobs\CreateKenyaEMRETL::dispatch($this->emr_backup_file)->onQueue('create_kenya_emr_etl');
                        } elseif ($this->emr_backup_file->emr_type->name == 'OpenMRS') {
                            \App\Jobs\DeletePreviousPatientVisits::dispatch($this->emr_backup_file)->onQueue('delete_previous_patient_visits');
                        }

                    }
                }
                elseif(\File::mimeType($this->emr_backup_file->backup_file) == 'application/zip') {
                    
                    $command5 = sprintf('unzip -p %s > %s',
                        $this->emr_backup_file->backup_file,
                        storage_path().'/app/uploads/'.$database_name.'.sql'
                    );

                    $process5 = new Process($command5);
                    $process5->setTimeout(null);
                    $process5->disableOutput();
                    $process5->mustRun();

                    if($process5->isSuccessful())
                    {
                        $command6 = sprintf('mysql --protocol=tcp --host=%s --port=%s --user=%s --password=%s --binary-mode --default-character-set=utf8 --comments --force %s < %s',
                            escapeshellarg(\Config::get('database.connections.pl_mysql.host')),
                            escapeshellarg(\Config::get('database.connections.pl_mysql.port')),
                            escapeshellarg(\Config::get('database.connections.pl_mysql.username')),
                            escapeshellarg(\Config::get('database.connections.pl_mysql.password')),
                            $database_name,
                            storage_path().'/app/uploads/'.$database_name.'.sql'
                        );

                        $process6 = new Process($command6);
                        $process6->setTimeout(null);
                        $process6->disableOutput();
                        $process6->mustRun();

                        if($process6->isSuccessful()) {

                            $this->emr_backup_file->date_restored = \Carbon\Carbon::now();
                            $this->emr_backup_file->restore_time = \Carbon\Carbon::now()->timestamp - $restore_start;
                            $this->emr_backup_file->database_name = $database_name;
                            $this->emr_backup_file->save();


                            if($this->emr_backup_file->emr_type->name == 'KenyaEMR') {
                                \App\Jobs\CreateKenyaEMRETL::dispatch($this->emr_backup_file)->onQueue('create_kenya_emr_etl');
                            } elseif ($this->emr_backup_file->emr_type->name == 'OpenMRS') {
                                \App\Jobs\DeletePreviousPatientVisits::dispatch($this->emr_backup_file)->onQueue('delete_previous_patient_visits');
                            }
                        }
                    }
                }
            }
        }
    }
}