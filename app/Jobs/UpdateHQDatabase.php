<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Symfony\Component\Process\Process;

class UpdateHQDatabase implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emr_backup_file;

    public function __construct(\App\Models\EmrBackupFile $emr_backup_file)
    {
        $this->emr_backup_file = $emr_backup_file;
    }

    public function handle()
    {
        if(isset($this->emr_backup_file->emr_type->name) && $this->emr_backup_file->emr_type->name == 'KenyaEMR'  && $this->emr_backup_file->database_name) {

            $database_name = $this->emr_backup_file->database_name;
            $hq_database_name_pieces = explode('_', $database_name);
            $hq_database_name = $hq_database_name_pieces[0].'_';

            $hq_host = \Config::get('database.connections.hq_mysql.host');
            $hq_port = \Config::get('database.connections.hq_mysql.port');
            $hq_username = \Config::get('database.connections.hq_mysql.username');
            $hq_password = \Config::get('database.connections.hq_mysql.password');
            
            $hq_connection = new \PDO("mysql:host=$hq_host", $hq_username,$hq_password);
            $hq_query_mysql = "SHOW DATABASES LIKE ?";
            $hq_statement = $hq_connection->prepare($hq_query_mysql);

            if ($hq_statement->execute([$hq_database_name.'%']) && $hq_database = $hq_statement->fetchColumn(0)) {
                
                $command = sprintf('mysqldump --protocol=tcp --host=%s --port=%s --user=%s --password=%s %s | mysql --protocol=tcp --host=%s --port=%s --user=%s --password=%s %s',
                    escapeshellarg(\Config::get('database.connections.pl_mysql.host')),
                    escapeshellarg(\Config::get('database.connections.pl_mysql.port')),
                    escapeshellarg(\Config::get('database.connections.pl_mysql.username')),
                    escapeshellarg(\Config::get('database.connections.pl_mysql.password')),
                    $database_name.'_dt',
                    $hq_host,
                    $hq_port,
                    $hq_username,
                    $hq_password,
                    $hq_database
                );

                $process = new Process($command);
                $process->setTimeout(null);
                $process->disableOutput();
                $process->mustRun();

                if($process->isSuccessful())
                {
                    \App\Jobs\DeIdentifyKenyaEMRDataTool::dispatch($hq_database);
                }
            }
        }
    }
}