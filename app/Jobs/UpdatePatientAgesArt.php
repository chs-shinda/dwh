<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class UpdatePatientAgesArt implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emr_backup_file;

    public function __construct(\App\Models\EmrBackupFile $emr_backup_file)
    {
        $this->emr_backup_file = $emr_backup_file;
    }

    public function handle()
    {
        $q = "UPDATE patients SET
                current_age = date_part('year', age(:emr_last_visit_date, dob)),
                age_at_last_visit = date_part('year', age(last_visit_date, dob)),
                age_at_hiv_enrollment = date_part('year', age(hiv_enrolled_date, dob)),
                age_at_art_start = date_part('year', age(art_start_date, dob)),
                current_on_art = CASE WHEN art_start_date IS NOT NULL AND current_on_care = true THEN true ELSE NULL END
            WHERE facility_id = :emr_facility_id";

        DB::update($q, [
            'emr_facility_id' => $this->emr_backup_file->facility_id,
            'emr_last_visit_date' => $this->emr_backup_file->last_visit_date
        ]);

        \App\Jobs\UpdatePatientCategorization::dispatch($this->emr_backup_file);
    }
}