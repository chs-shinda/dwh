<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class UpdatePatientCategorization implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emr_backup_file;

    public function __construct(\App\Models\EmrBackupFile $emr_backup_file)
    {
        $this->emr_backup_file = $emr_backup_file;
    }

    public function handle()
    {
        \App\Models\Patient::where('facility_id', $this->emr_backup_file->facility_id)->update([
            'pc_well' => null,
            'pc_advanced' => null,
            'pc_stable' => null,
            'pc_unstable' => null
        ]);

        $patients = \App\Models\Patient::where('facility_id', $this->emr_backup_file->facility_id)->where('current_on_care', true)->get();

        foreach ($patients as $patient) {
            
            $changed = false;
            
            if($patient->art_start_date && $patient->last_visit_date && \Carbon\Carbon::createFromFormat('Y-m-d', $patient->last_visit_date)->diffInMonths(\Carbon\Carbon::createFromFormat('Y-m-d', $patient->art_start_date)) < 12) {

                if(($patient->initial_who_stage == 'who stage 1' || $patient->initial_who_stage == 'who stage 2') && ($patient->initial_cd > 200 || $patient->current_cd4_percent > 25)) {
                    $patient->pc_well = 1;
                    $changed = true;
                }
                
                if(($patient->initial_who_stage == 'who stage 3' || $patient->initial_who_stage == 'who stage 4') || ($patient->initial_cd < 200 || $patient->current_cd4_percent < 25)) {
                    $patient->pc_advanced = 1;
                    $changed = true;
                }

            }
            
            if($patient->art_start_date && $patient->last_visit_date && \Carbon\Carbon::createFromFormat('Y-m-d', $patient->last_visit_date)->diffInMonths(\Carbon\Carbon::createFromFormat('Y-m-d', $patient->art_start_date)) >= 12) {

                if((\Carbon\Carbon::createFromFormat('Y-m-d', $patient->last_visit_date)->diffInMonths(\Carbon\Carbon::createFromFormat('Y-m-d', $patient->art_start_date)) >= 12) && ($patient->tb_enrolled_date == '' || \Carbon\Carbon::createFromFormat('Y-m-d', $patient->last_visit_date)->diffInMonths(\Carbon\Carbon::createFromFormat('Y-m-d', $patient->tb_enrolled_date)) > 6) && $patient->current_vl < 1000 && $patient->age_at_last_visit >= 20) {
                    $patient->pc_stable = 1;
                } else {
                    $patient->pc_unstable = 1;
                }

                $changed = true;
            }

            if($changed) {
                $patient->save();
            }
        }
    }
}