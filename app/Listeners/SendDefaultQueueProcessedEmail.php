<?php

namespace App\Listeners;

use App\Events\DefaultQueueProcessed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendDefaultQueueProcessedEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DefaultQueueProcessed  $event
     * @return void
     */
    public function handle(DefaultQueueProcessed $event)
    {
        //
    }
}
