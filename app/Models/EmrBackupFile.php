<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmrBackupFile extends Model
{
	use SoftDeletes;

	protected $table = 'emr_backup_files';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	protected $dates = ['deleted_at'];

	public $fillable = [
		'emr_type_id',
		'facility_id',
		'database_name',
		'backup_file',
		'date_restored',
		'restore_time',
		'date_processed',
		'process_time',
		'created_by',
		'deleted_by',
		'old_id',
	];

	public static $rules = [
		'emr_type_id' => 'required',
		'facility_id' => 'required',
	];

	public function emr_type()
    {
        return $this->belongsTo('App\Models\EmrType');
    }

    public function facility()
    {
        return $this->belongsTo('App\Models\Facility');
    }
}