<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmrType extends Model
{
	use SoftDeletes;

	protected $table = 'emr_types';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	protected $dates = ['deleted_at'];

	public $fillable = [
		'name',
		'database_dialect',
		'created_by',
		'deleted_by',
		'old_id',
	];

	public static $rules = [
		'name' => 'required',
	];
}