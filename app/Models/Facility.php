<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Facility extends Model
{
	use SoftDeletes;

    protected $table = 'facilities';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'code',
        'name',
        'county',
        'sub_county',
        'ward',
        'constituency',
        'longitude',
        'latitude',
        'created_by',
        'deleted_by',
        'old_id',
    ];

    public static $rules = [
        'code' => 'required',
        'name' => 'required',
    ];
}