<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
	protected $table = 'patients';

	public $timestamps = false;

	public $fillable = [
		'facility_id',
		'emr_patient_id',
		'ccc',
		'sex',
		'dob',
		'dob_estimated',
		'marital_status',
		'education_level',
		'dead',
		'dod',
		'cause_of_death',
		'hiv_positive_confirmed_date',
		'hiv_positive_confirmed_facility',
		'registration_date',
		'hiv_enrolled_date',
		'tb_enrolled_date',
		'mch_child_enrolled_date',
		'mch_mother_enrolled_date',
		'art_start_date',
		'entry_point',
		'transfer_in_date',
		'transferring_facility',
		'transferring_facility_district',
		'transferring_facility_art_start_date',
		'first_visit_date',
		'last_visit_date',
		'initial_regimen',
		'initial_regimen_name',
		'initial_regimen_line',
		'initial_regimen_date',
		'current_regimen',
		'current_regimen_name',
		'current_regimen_line',
		'current_regimen_date',
		'initial_who_stage',
		'initial_who_stage_date',
		'current_who_stage',
		'current_who_stage_date',
		'initial_cd4',
		'initial_cd4_date',
		'initial_cd4_percent',
		'initial_cd4_percent_date',
		'current_cd4',
		'current_cd4_date',
		'current_cd4_percent',
		'current_cd4_percent_date',
		'initial_vl',
		'initial_vl_date',
		'current_vl',
		'current_vl_date',
		'transfer_out_date',
		'transfer_out_facility',
		'next_appointment_date',
	];

	public static $rules = [
		'facility_id' => 'required',
	];
}