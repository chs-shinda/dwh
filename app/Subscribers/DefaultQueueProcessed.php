<?php

namespace App\Subscribers;

use App\Mail\DefaultQueueProcessed as DefaultQueueProcessedMailer;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Queue\Events\Looping;
use Illuminate\Support\Facades\Mail;
use Queue;

class DefaultQueueProcessed
{
    public function subscribe(Dispatcher $events)
    {
        $events->listen(Looping::class, self::class . '@on_queue_loop');
    }

    public function on_queue_loop()
    {
        if (Queue::size('get_patient_visits') === 0) {
            Mail::to('jondenge@gmail.com')->queue((new DefaultQueueProcessedMailer())->onQueue('emails'));
        }
    }
}