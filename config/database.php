<?php

return [

    'default' => env('DB_CONNECTION', 'mysql'),

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'powerbi' => [
            'driver' => 'mysql',
            'host' => env('POWERBI_DB_HOST', '127.0.0.1'),
            'port' => env('POWERBI_DB_PORT', '3306'),
            'database' => env('POWERBI_DB_DATABASE', 'forge'),
            'username' => env('POWERBI_DB_USERNAME', 'forge'),
            'password' => env('POWERBI_DB_PASSWORD', ''),
            'unix_socket' => env('POWERBI_DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'pl_mysql' => [
            'driver' => 'mysql',
            'host' => env('PL_MYSQL_DB_HOST', '127.0.0.1'),
            'port' => env('PL_MYSQL_DB_PORT', '3306'),
            'database' => env('PL_MYSQL_DB_DATABASE', 'forge'),
            'username' => env('PL_MYSQL_DB_USERNAME', 'forge'),
            'password' => env('PL_MYSQL_DB_PASSWORD', ''),
            'unix_socket' => env('PL_MYSQL_DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],

        'hq_mysql' => [
            'driver' => 'mysql',
            'host' => env('HQ_MYSQL_DB_HOST', '127.0.0.1'),
            'port' => env('HQ_MYSQL_DB_PORT', '3306'),
            'database' => env('HQ_MYSQL_DB_DATABASE', 'forge'),
            'username' => env('HQ_MYSQL_DB_USERNAME', 'forge'),
            'password' => env('HQ_MYSQL_DB_PASSWORD', ''),
            'unix_socket' => env('HQ_MYSQL_DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],

        'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'sqlsrv' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '1433'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
        ],

    ],

    'migrations' => 'migrations',

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

        'queue' => [
            'host' => env('REDIS_QUEUE_HOST', '127.0.0.1'),
            'password' => env('REDIS_QUEUE_PASSWORD', null),
            'port' => env('REDIS_QUEUE_PORT', 6379),
            'database' => 0,
        ],

        'cache' => [
            'host' => env('REDIS_CACHE_HOST', '127.0.0.1'),
            'password' => env('REDIS_CACHE_PASSWORD', null),
            'port' => env('REDIS_CACHE_PORT', 6379),
            'database' => 0,
        ],

    ],

];
