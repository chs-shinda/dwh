<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmrBackupFilesTable extends Migration
{
    public function up()
    {
        Schema::create('emr_backup_files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emr_type_id');
            $table->integer('facility_id');
            $table->string('database_name')->nullable();
            $table->string('backup_file')->nullable();
            $table->dateTime('date_restored')->nullable();
            $table->integer('restore_time')->nullable();
            $table->dateTime('date_processed')->nullable();
            $table->integer('process_time')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->integer('old_id')->nullable();
            
            $table->timestamps();
            $table->softDeletes();

            $table->index('facility_id');
            $table->index('deleted_at');

        });
    }
    
    public function down()
    {
        Schema::dropIfExists('emr_backup_files');
    }
}
