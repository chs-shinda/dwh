<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmrTypesTable extends Migration
{
    public function up()
    {
        Schema::create('emr_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('database_dialect')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->integer('old_id')->nullable();
            
            $table->timestamps();
            $table->softDeletes();

            $table->index('name');
            $table->index('deleted_at');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('emr_types');
    }
}
