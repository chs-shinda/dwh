<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('facility_id');
            $table->integer('emr_patient_id')->nullable();
            $table->string('ccc')->nullable();
            $table->string('sex')->nullable();
            $table->date('dob')->nullable();
            $table->boolean('dob_estimated')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('education_level')->nullable();
            $table->boolean('dead')->nullable();
            $table->date('dod')->nullable();
            $table->string('cause_of_death')->nullable();
            $table->date('hiv_positive_confirmed_date')->nullable();
            $table->string('hiv_positive_confirmed_facility')->nullable();
            $table->date('registration_date')->nullable();
            $table->date('hiv_enrolled_date')->nullable();
            $table->date('tb_enrolled_date')->nullable();
            $table->date('mch_child_enrolled_date')->nullable();
            $table->date('mch_mother_enrolled_date')->nullable();
            $table->date('art_start_date')->nullable();
            $table->string('entry_point')->nullable();
            $table->date('transfer_in_date')->nullable();
            $table->string('transferring_facility')->nullable();
            $table->string('transferring_facility_district')->nullable();
            $table->date('transferring_facility_art_start_date')->nullable();
            $table->date('first_visit_date')->nullable();
            $table->date('last_visit_date')->nullable();
            $table->string('initial_regimen')->nullable();
            $table->string('initial_regimen_name')->nullable();
            $table->string('initial_regimen_line')->nullable();
            $table->date('initial_regimen_date')->nullable();
            $table->string('current_regimen')->nullable();
            $table->string('current_regimen_name')->nullable();
            $table->string('current_regimen_line')->nullable();
            $table->date ('current_regimen_date')->nullable();
            $table->string('initial_who_stage')->nullable();
            $table->date('initial_who_stage_date')->nullable();
            $table->string('current_who_stage')->nullable();
            $table->date('current_who_stage_date')->nullable();
            $table->double('initial_cd4')->nullable();
            $table->date('initial_cd4_date')->nullable();
            $table->double('initial_cd4_percent')->nullable();
            $table->date('initial_cd4_percent_date')->nullable();
            $table->double('current_cd4')->nullable();
            $table->date('current_cd4_date')->nullable();
            $table->double('current_cd4_percent')->nullable();
            $table->date('current_cd4_percent_date')->nullable();
            $table->double('initial_vl')->nullable();
            $table->date('initial_vl_date')->nullable();
            $table->double('current_vl')->nullable();
            $table->date('current_vl_date')->nullable();

            $table->index('facility_id');
            $table->index('emr_patient_id');
            $table->index('ccc');
            $table->index('sex');
            $table->index('dob');
            $table->index('dead');
            $table->index('hiv_positive_confirmed_date');
            $table->index('hiv_enrolled_date');
            $table->index('art_start_date');
            $table->index('first_visit_date');
            $table->index('last_visit_date');
            $table->index('initial_regimen_name');
            $table->index('current_regimen_name');
            $table->index('initial_who_stage');
            $table->index('current_who_stage');
            $table->index('initial_cd4');
            $table->index('current_cd4');
            $table->index('initial_vl');
            $table->index('current_vl');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
