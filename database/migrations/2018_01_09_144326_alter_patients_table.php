<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPatientsTable extends Migration
{
    public function up()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->date('transfer_out_date')->nullable();
            $table->string('transfer_out_facility')->nullable();
            $table->date('next_appointment_date')->nullable();
            $table->index('transfer_out_date');
            $table->index('next_appointment_date');
        });
    }
    
    public function down()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->dropColumn('next_appointment_date');
            $table->dropColumn('transfer_out_facility');
            $table->dropColumn('transfer_out_date');
        });
    }
}
