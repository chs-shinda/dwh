<?php

use App\Models\EmrType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmrTypesTable extends Migration
{
    protected $emr_types = [
        [ 'name' => 'KenyaEMR', 'database_dialect' => 'mysql' ],
    ];

    public function up()
    {
        foreach ($this->emr_types as $emr_type) {
            $et = EmrType::where('name', $emr_type['name'])->first();
            if(!$et) {
                EmrType::create($emr_type);
            }
        }
    }
    
    public function down()
    {
        foreach ($this->emr_types as $emr_type) {
            EmrType::where('name', $emr_type['name'])->delete();
        }
    }
}
