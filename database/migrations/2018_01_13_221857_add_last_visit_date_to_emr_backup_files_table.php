<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastVisitDateToEmrBackupFilesTable extends Migration
{
    public function up()
    {
        Schema::table('emr_backup_files', function (Blueprint $table) {
            $table->date('last_visit_date')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('emr_backup_files', function (Blueprint $table) {
            $table->dropColumn('last_visit_date');
        });
    }
}
