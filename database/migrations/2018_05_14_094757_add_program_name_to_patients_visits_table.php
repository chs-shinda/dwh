<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProgramNameToPatientsVisitsTable extends Migration
{
    public function up()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->string('program_name')->nullable();
            $table->boolean('current_on_care')->nullable();
            $table->integer('age_at_last_visit')->nullable();
            $table->string('transfer_in_facility')->nullable();
            $table->string('transfer_in_district')->nullable();
            $table->date('transfer_in_art_start_date')->nullable();
            $table->dropColumn('transferring_facility');
            $table->dropColumn('transferring_facility_district');
            $table->dropColumn('transferring_facility_art_start_date');

        });
        Schema::table('visits', function (Blueprint $table) {
            $table->string('program_name')->nullable();
            $table->string('regimen_name')->nullable();
            $table->string('regimen_line')->nullable();
            $table->text('reason_discontinued')->nullable();
            $table->string('transfer_in_facility')->nullable();
            $table->string('transfer_in_district')->nullable();
            $table->date('transfer_in_art_start_date')->nullable();
            $table->date('transfer_out_date')->nullable();
            $table->string('transfer_out_facility')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->dropColumn('program_name');
            $table->dropColumn('current_on_care');
            $table->dropColumn('age_at_last_visit');
            $table->dropColumn('transfer_in_facility');
            $table->dropColumn('transfer_in_district');
            $table->dropColumn('transfer_in_art_start_date');
            $table->string('transferring_facility')->nullable();
            $table->string('transferring_facility_district')->nullable();
            $table->date('transferring_facility_art_start_date')->nullable();
        });
        Schema::table('visits', function (Blueprint $table) {
            $table->dropColumn('program_name');
            $table->dropColumn('regimen_name');
            $table->dropColumn('regimen_line');
            $table->dropColumn('reason_discontinued');
            $table->dropColumn('transfer_in_facility');
            $table->dropColumn('transfer_in_district');
            $table->dropColumn('transfer_in_art_start_date');
            $table->dropColumn('transfer_out_date');
            $table->dropColumn('transfer_out_facility');
        });
    }
}
