<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViralLoadLdlToVisitsTable extends Migration
{
    public function up()
    {
        Schema::table('visits', function (Blueprint $table) {
            $table->string('viral_load_ldl')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('visits', function (Blueprint $table) {
            $table->dropColumn('viral_load_ldl');
        });
    }
}
