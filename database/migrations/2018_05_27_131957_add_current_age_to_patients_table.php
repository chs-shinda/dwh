<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrentAgeToPatientsTable extends Migration
{
    public function up()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->boolean('current_on_art')->nullable();
            $table->integer('current_age')->nullable();

        });
    }
    
    public function down()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->dropColumn('current_on_art');
            $table->dropColumn('current_age');
        });
    }
}
