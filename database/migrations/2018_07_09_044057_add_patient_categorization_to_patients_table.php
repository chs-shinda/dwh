<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPatientCategorizationToPatientsTable extends Migration
{
    public function up()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->double('initial_bmi')->nullable();
            $table->date('initial_bmi_date')->nullable();
            $table->double('current_bmi')->nullable();
            $table->date('current_bmi_date')->nullable();
            $table->boolean('pc_well')->nullable();
            $table->boolean('pc_advanced')->nullable();
            $table->boolean('pc_stable')->nullable();
            $table->boolean('pc_unstable')->nullable();
            $table->integer('age_at_hiv_enrollment')->nullable();
            $table->integer('age_at_art_start')->nullable();
            $table->date('initial_ipt_start_date')->nullable();
            $table->date('current_ipt_start_date')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->dropColumn('initial_bmi');
            $table->dropColumn('initial_bmi_date');
            $table->dropColumn('current_bmi');
            $table->dropColumn('current_bmi_date');
            $table->dropColumn('pc_well');
            $table->dropColumn('pc_advanced');
            $table->dropColumn('pc_stable');
            $table->dropColumn('pc_unstable');
            $table->dropColumn('age_at_hiv_enrollment');
            $table->dropColumn('age_at_art_start');
            $table->dropColumn('initial_ipt_start_date');
            $table->dropColumn('current_ipt_start_date');
        });
    }
}
