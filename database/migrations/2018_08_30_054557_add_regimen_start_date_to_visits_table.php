<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRegimenStartDateToVisitsTable extends Migration
{
    public function up()
    {
        Schema::table('visits', function (Blueprint $table) {
            $table->date('regimen_start_date')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('visits', function (Blueprint $table) {
            $table->dropColumn('regimen_start_date');
        });
    }
}
