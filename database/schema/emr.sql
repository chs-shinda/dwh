drop table if exists `patients`;

create table `patients` (
    `id` bigint not null auto_increment,
    `facility` varchar(256),
    `mfl` int,
    `emr_patient_id` int,
    `ccc` varchar(256),
    `sex` varchar(10),
    `dob` date,
    `dob_estimated` boolean,
    `marital_status` varchar(256),
    `education_level` varchar(256),
    `dead` boolean,
    `dod` date,
    `cause_of_death` varchar(256),
    `hiv_positive_confirmed_date` date,
    `hiv_positive_confirmed_facility` text,
    `hiv_enrolled_date` date,
    `art_start_date` date,
    `entry_point` varchar(256),
    `transfer_in_date` date,
    `transferring_facility` varchar(256),
    `transferring_facility_district` varchar(256),
    `transferring_facility_art_start_date` date,
    `transfer_out_date` date,
    `transfer_out_facility` varchar(256),
    `first_visit_date` date,
    `last_visit_date` date,
    `initial_regimen` varchar(256),
    `initial_regimen_line` varchar(256),
    `initial_regimen_date` date,
    `current_regimen` varchar(256),
    `current_regimen_line` varchar(256),
    `current_regimen_date` date,
    `initial_who_stage` varchar(256),
    `initial_who_stage_date` date,
    `current_who_stage` varchar(256),
    `current_who_stage_date` date,
    `initial_cd4` varchar(256),
    `initial_cd4_date` date,
    `current_cd4` varchar(256),
    `current_cd4_date` date,
    `initial_vl` varchar(256),
    `initial_vl_date` date,
    `current_vl` varchar(256),
    `current_vl_date` date,
    primary key (`id`)
);

alter table `patients` add index (`mfl`);
alter table `patients` add index (`ccc`);
alter table `patients` add index (`sex`);
alter table `patients` add index (`dob`);
alter table `patients` add index (`dead`);
alter table `patients` add index (`art_start_date`);
alter table `patients` add index (`current_regimen`);

drop table if exists `visits`;

create table `visits` (
    `id` bigint not null auto_increment,
    `facility` varchar(256),
    `mfl` int,
    `emr_patient_id` int,
    `ccc` varchar(256),
    `emr_visit_id` int,
    `visit_date` date,
    `visit_type` varchar(256),
    `regimen` varchar(256),
    `pregnancy_status` varchar(256),
    `arv_adherence` varchar(256),
    `weight` varchar(256),
    `height` varchar(256),
    `family_planning_status` varchar(256),
    `tb_status` varchar(256),
    `who_stage` varchar(256),
    `viral_load` varchar(256),
    `next_appointment_date` date,
    primary key (`id`)
);

alter table `visits` add index (`emr_patient_id`);
alter table `visits` add index (`emr_visit_id`);
alter table `visits` add index (`visit_date`);
alter table `visits` add index (`next_appointment_date`);