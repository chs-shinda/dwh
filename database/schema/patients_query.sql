SELECT
    e.upn AS ccc,
    a.person_id AS emr_patient_id,
    CASE WHEN a.gender = 'M' THEN 'male' WHEN a.gender = 'F' THEN 'female' ELSE 'other' END AS sex,
    CASE WHEN a.birthdate IS NULL THEN a.birthdate ELSE DATE_FORMAT(a.birthdate,'%Y-%m-%d') END AS dob,
    a.birthdate_estimated AS dob_estimated,
    c.marital_status,
    c.education_level,
    a.dead,
    CASE WHEN a.death_date IS NULL THEN a.death_date ELSE DATE_FORMAT(a.death_date,'%Y-%m-%d') END AS dod,
    i.cause_of_death,
    CASE WHEN d.hiv_positive_confirmed_date IS NULL THEN d.hiv_positive_confirmed_date ELSE DATE_FORMAT(d.hiv_positive_confirmed_date,'%Y-%m-%d') END AS hiv_positive_confirmed_date,  
    d.hiv_positive_confirmed_facility,
    CASE WHEN h.registration_date IS NULL THEN h.registration_date ELSE DATE_FORMAT(h.registration_date,'%Y-%m-%d') END AS registration_date,
    CASE WHEN h.hiv_enrolled_date IS NULL THEN h.hiv_enrolled_date ELSE DATE_FORMAT(h.hiv_enrolled_date,'%Y-%m-%d') END AS hiv_enrolled_date,
    CASE WHEN h.tb_enrolled_date IS NULL THEN h.tb_enrolled_date ELSE DATE_FORMAT(h.tb_enrolled_date,'%Y-%m-%d') END AS tb_enrolled_date,
    CASE WHEN h.mch_child_enrolled_date IS NULL THEN h.mch_child_enrolled_date ELSE DATE_FORMAT(h.mch_child_enrolled_date,'%Y-%m-%d') END AS mch_child_enrolled_date,
    CASE WHEN h.mch_mother_enrolled_date IS NULL THEN h.mch_mother_enrolled_date ELSE DATE_FORMAT(h.mch_mother_enrolled_date,'%Y-%m-%d') END AS mch_mother_enrolled_date,
    CASE WHEN COALESCE(f.initial_regimen_start_date, d.transferring_facility_art_start_date, NULL) IS NULL THEN NULL ELSE DATE_FORMAT(COALESCE(f.initial_regimen_start_date, d.transferring_facility_art_start_date),'%Y-%m-%d') END AS art_start_date, 
    c.entry_point,
    CASE WHEN d.transfer_in_date IS NULL THEN d.transfer_in_date ELSE DATE_FORMAT(d.transfer_in_date,'%Y-%m-%d') END AS transfer_in_date,
    d.transferring_facility,
    d.transferring_facility_district,
    CASE WHEN d.transferring_facility_art_start_date IS NULL THEN d.transferring_facility_art_start_date ELSE DATE_FORMAT(d.transferring_facility_art_start_date,'%Y-%m-%d') END AS transferring_facility_art_start_date,
    CASE WHEN g.first_visit_date IS NULL THEN g.first_visit_date ELSE DATE_FORMAT(g.first_visit_date,'%Y-%m-%d') END AS first_visit_date,
    CASE WHEN g.last_visit_date IS NULL THEN g.last_visit_date ELSE DATE_FORMAT(g.last_visit_date,'%Y-%m-%d') END AS last_visit_date,
    f.initial_regimen_name,  
    f.initial_regimen_line,
    CASE WHEN f.initial_regimen_start_date IS NULL THEN f.initial_regimen_start_date ELSE DATE_FORMAT(f.initial_regimen_start_date,'%Y-%m-%d') END AS initial_regimen_date,  
    f.current_regimen_name,  
    f.current_regimen_line,  
    CASE WHEN f.current_regimen_start_date IS NULL THEN f.current_regimen_start_date ELSE DATE_FORMAT(f.current_regimen_start_date,'%Y-%m-%d') END AS current_regimen_date, 
    c.initial_who_stage,  
    CASE WHEN c.initial_who_stage_date IS NULL THEN c.initial_who_stage_date ELSE DATE_FORMAT(c.initial_who_stage_date,'%Y-%m-%d') END AS initial_who_stage_date,  
    c.current_who_stage,  
    CASE WHEN c.current_who_stage_date IS NULL THEN c.current_who_stage_date ELSE DATE_FORMAT(c.current_who_stage_date,'%Y-%m-%d') END AS current_who_stage_date,  
    d.initial_cd4,  
    CASE WHEN d.initial_cd4_date IS NULL THEN d.initial_cd4_date ELSE DATE_FORMAT(d.initial_cd4_date,'%Y-%m-%d') END AS initial_cd4_date,  
    d.initial_cd4_percent,  
    CASE WHEN d.initial_cd4_percent_date IS NULL THEN d.initial_cd4_percent_date ELSE DATE_FORMAT(d.initial_cd4_percent_date,'%Y-%m-%d') END AS initial_cd4_percent_date,  
    d.current_cd4,  
    CASE WHEN d.current_cd4_date IS NULL THEN d.current_cd4_date ELSE DATE_FORMAT(d.current_cd4_date,'%Y-%m-%d') END AS current_cd4_date,  
    d.current_cd4_percent,  
    CASE WHEN d.current_cd4_percent_date IS NULL THEN d.current_cd4_percent_date ELSE DATE_FORMAT(d.current_cd4_percent_date,'%Y-%m-%d') END AS current_cd4_percent_date,  
    d.initial_vl,  
    CASE WHEN d.initial_vl_date IS NULL THEN d.initial_vl_date ELSE DATE_FORMAT(d.initial_vl_date,'%Y-%m-%d') END AS initial_vl_date,  
    d.current_vl,  
    CASE WHEN d.current_vl_date IS NULL THEN d.current_vl_date ELSE DATE_FORMAT(d.current_vl_date,'%Y-%m-%d') END AS current_vl_date
FROM person a
INNER JOIN patient b ON b.patient_id = a.person_id
LEFT JOIN (
    SELECT
        a.person_id AS patient_id,
        max(if(a.concept_id=1054,lower(b.name),null)) AS marital_status,  
        max(if(a.concept_id=1712,lower(b.name),null)) AS education_level,  
        max(if(a.concept_id=160540,lower(b.name),null)) AS entry_point,  
        min(if(a.concept_id=5356,lower(b.name),null)) AS initial_who_stage,  
        min(if(a.concept_id=5356,a.obs_datetime,null)) AS initial_who_stage_date,  
        max(if(a.concept_id=5356,lower(b.name),null)) AS current_who_stage,  
        max(if(a.concept_id=5356,a.obs_datetime,null)) AS current_who_stage_date 
    FROM obs a  
    LEFT JOIN concept_name b ON b.concept_id = a.value_coded AND b.concept_name_type = 'FULLY_SPECIFIED' AND b.locale = 'en' AND b.voided = 0  
    WHERE a.concept_id IN (1054,1712,160540,5356) AND a.voided = 0  
    GROUP BY person_id  
) c ON c.patient_id = b.patient_id  
LEFT JOIN ( 
    SELECT  
        a.person_id AS patient_id,
        max(if(a.concept_id=160534,a.value_datetime,null)) AS transfer_in_date,
        max(if(a.concept_id=160535,lower(a.value_text),null)) AS transferring_facility,
        max(if(a.concept_id=161551,lower(a.value_text),null)) AS transferring_facility_district,
        max(if(a.concept_id=159599,a.value_datetime,null)) AS transferring_facility_art_start_date,
        max(if(a.concept_id=160554,a.value_datetime,null)) AS hiv_positive_confirmed_date,  
        max(if(a.concept_id=160632,lower(a.value_text),null)) AS hiv_positive_confirmed_facility,  
        max(if(a.concept_id=160533,a.value_boolean,null)) AS previous_arv_status,  
        min(if(a.concept_id=5497,a.value_numeric,null)) AS initial_cd4,  
        min(if(a.concept_id=5497,a.obs_datetime,null)) AS initial_cd4_date,  
        min(if(a.concept_id=730,a.value_numeric,null)) AS initial_cd4_percent,  
        min(if(a.concept_id=730,a.obs_datetime,null)) AS initial_cd4_percent_date, 
        max(if(a.concept_id=5497,a.value_numeric,null)) AS current_cd4,  
        max(if(a.concept_id=5497,a.obs_datetime,null)) AS current_cd4_date,  
        max(if(a.concept_id=730,a.value_numeric,null)) AS current_cd4_percent,  
        max(if(a.concept_id=730,a.obs_datetime,null)) AS current_cd4_percent_date,  
        min(if(a.concept_id=856,a.value_numeric,null)) AS initial_vl,  
        min(if(a.concept_id=856,a.obs_datetime,null)) AS initial_vl_date,  
        max(if(a.concept_id=856,a.value_numeric,null)) AS current_vl,  
        max(if(a.concept_id=856,a.obs_datetime,null)) AS current_vl_date  
    FROM obs a
    WHERE a.concept_id IN (160555,159599,160554,160632,160533,5497,730,856, 160534, 160535, 161551) AND a.voided = 0
    GROUP BY person_id
) d ON d.patient_id = b.patient_id
LEFT JOIN (
    SELECT
        a.patient_id,
        max(if(b.uuid='05ee9cf4-7242-4a17-b4d4-00f707265c8a',lower(a.identifier),null)) AS upn,
        max(if(b.uuid='d8ee3b8c-a8fc-4d6b-af6a-9423be5f8906',lower(a.identifier),null)) AS district_reg_number,
        max(if(b.uuid='c4e3caca-2dcc-4dc4-a8d9-513b6e63af91',lower(a.identifier),null)) AS tb_treatment_number,
        max(if(b.uuid='b4d66522-11fc-45c7-83e3-39a1af21ae0d',lower(a.identifier),null)) AS patient_clinic_number,
        max(if(b.uuid='49af6cdc-7968-4abb-bf46-de10d7f4859f',lower(a.identifier),null)) AS national_id,
        max(if(b.uuid='0691f522-dd67-4eeb-92c8-af5083baf338',lower(a.identifier),null)) AS hei_id
    FROM patient_identifier a  
    JOIN patient_identifier_type b ON a.identifier_type=b.patient_identifier_type_id  
    WHERE voided = 0  
    GROUP BY a.patient_id  
) e ON e.patient_id = b.patient_id
LEFT JOIN (
    SELECT
        a.patient_id,
        date(min(a.start_date)) AS initial_regimen_start_date,
        lower(min(a.regimen)) AS initial_regimen,
        lower(min(a.regimen_name)) AS initial_regimen_name,
        lower(min(a.regimen_line)) AS initial_regimen_line,
        min(a.date_discontinued) AS initial_regimen_discontinued_date,
        lower(min(a.discontinued_reason)) AS initial_regimen_discontinued_reason,
        date(max(a.start_date)) AS current_regimen_start_date,
        lower(max(a.regimen)) AS current_regimen,
        lower(max(a.regimen_name)) AS current_regimen_name,
        lower(max(a.regimen_line)) AS current_regimen_line,
        max(a.date_discontinued) AS current_regimen_discontinued_date,
        lower(max(a.discontinued_reason)) AS current_regimen_discontinued_reason
    FROM (
        SELECT
            orders.*
        FROM encounter e 
        INNER JOIN (
            SELECT 
                orders.uuid,
                orders.patient_id, 
                orders.start_date,
                date(orders.discontinued_date) AS date_discontinued,
                orders.discontinued_reason AS discontinued_reason,
                enc.encounter_datetime,
                group_concat(distinct cn.name ORDER BY orders.order_id) AS regimen,
                (CASE 
                    WHEN FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LOPINAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN 'ABC+3TC+LPV/r'
                    WHEN FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('DIDANOSINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LOPINAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN 'ABC+ddI+LPV/r'
                    WHEN FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('DARUNAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN 'AZT+3TC+DRV/r'
                    WHEN FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('DARUNAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN 'ABC+3TC+DRV/r'

                    WHEN FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LOPINAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN 'AZT+3TC+LPV/r'
                    WHEN FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ATAZANAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN 'AZT+3TC+ATV/r'
                    WHEN FIND_IN_SET('TENOFOVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LOPINAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN 'TDF+3TC+LPV/r'
                    WHEN FIND_IN_SET('TENOFOVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LOPINAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN 'TDF+ABC+LPV/r'
                    WHEN FIND_IN_SET('TENOFOVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ATAZANAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN 'TDF+3TC+ATV/r'
                    WHEN FIND_IN_SET('STAVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LOPINAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN 'd4T+3TC+LPV/r'
                    WHEN FIND_IN_SET('STAVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LOPINAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN 'd4T+ABC+LPV/r'
                    WHEN FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('DIDANOSINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LOPINAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN 'AZT+ddI+LPV/r'
                    WHEN FIND_IN_SET('TENOFOVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LOPINAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN 'TDF+AZT+LPV/r'
                    WHEN FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LOPINAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN 'AZT+ABC+LPV/r'
                    
                    WHEN FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('NEVIRAPINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN 'AZT+3TC+NVP'
                    WHEN FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('EFAVIRENZ', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN 'AZT+3TC+EFV'
                    WHEN FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN 'AZT+3TC+ABC'
                    
                    WHEN FIND_IN_SET('TENOFOVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('NEVIRAPINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN 'TDF+3TC+NVP'
                    WHEN FIND_IN_SET('TENOFOVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('EFAVIRENZ', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN 'TDF+3TC+EFV'
                    WHEN FIND_IN_SET('TENOFOVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN 'TDF+3TC+ABC'
                    WHEN FIND_IN_SET('TENOFOVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN 'TDF+3TC+AZT'
                    
                    WHEN FIND_IN_SET('STAVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('NEVIRAPINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN 'd4T+3TC+NVP'
                    WHEN FIND_IN_SET('STAVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('EFAVIRENZ', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN 'd4T+3TC+EFV'
                    WHEN FIND_IN_SET('STAVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN 'd4T+3TC+ABC'
                    
                    WHEN FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('NEVIRAPINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN 'ABC+3TC+NVP'
                    WHEN FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('EFAVIRENZ', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN 'ABC+3TC+EFV'
                    WHEN FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN 'ABC+3TC+AZT'
                END) AS regimen_name,
                (CASE 
                    WHEN FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LOPINAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN '2nd Line'
                    WHEN FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('DIDANOSINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LOPINAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN '2nd Line'
                    WHEN FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('DARUNAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN '2nd Line'
                    WHEN FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('DARUNAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN '2nd Line'
                    WHEN FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LOPINAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN '2nd Line'
                    WHEN FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ATAZANAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN '2nd Line'
                    WHEN FIND_IN_SET('TENOFOVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LOPINAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN '2nd Line'
                    WHEN FIND_IN_SET('TENOFOVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LOPINAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN '2nd Line'
                    WHEN FIND_IN_SET('TENOFOVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ATAZANAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN '2nd Line'
                    WHEN FIND_IN_SET('STAVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LOPINAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN '2nd Line'
                    WHEN FIND_IN_SET('STAVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LOPINAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN '2nd Line'
                    WHEN FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('DIDANOSINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LOPINAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN '2nd Line'
                    WHEN FIND_IN_SET('TENOFOVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LOPINAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN '2nd Line'
                    WHEN FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LOPINAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('RITONAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 THEN '2nd Line'
                    
                    WHEN FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('NEVIRAPINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN '1st Line'
                    WHEN FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('EFAVIRENZ', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN '1st Line'
                    WHEN FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN '2nd Line'
                    
                    WHEN FIND_IN_SET('TENOFOVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('NEVIRAPINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN '1st Line'
                    WHEN FIND_IN_SET('TENOFOVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('EFAVIRENZ', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN '1st Line'
                    WHEN FIND_IN_SET('TENOFOVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN '2nd Line'
                    WHEN FIND_IN_SET('TENOFOVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN '2nd Line'
                    
                    WHEN FIND_IN_SET('STAVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('NEVIRAPINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN '1st Line'
                    WHEN FIND_IN_SET('STAVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('EFAVIRENZ', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN '1st Line'
                    WHEN FIND_IN_SET('STAVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN '2nd Line'
                    
                    WHEN FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('NEVIRAPINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN '1st Line'
                    WHEN FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('EFAVIRENZ', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN '1st Line'
                    WHEN FIND_IN_SET('ABACAVIR', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('LAMIVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0 AND FIND_IN_SET('ZIDOVUDINE', group_concat(distinct cn.name ORDER BY orders.order_id)) > 0  THEN '1st Line'
                END) AS regimen_line,
                orders.voided 
            FROM orders orders
            LEFT OUTER JOIN concept_name cn ON orders.concept_id = cn.concept_id AND cn.locale='en' AND cn.concept_name_type='FULLY_SPECIFIED'
            JOIN (SELECT patient_id,encounter_datetime FROM encounter )enc ON enc.patient_id=orders.patient_id
            WHERE orders.voided=0 AND orders.start_date<=enc.encounter_datetime AND (enc.encounter_datetime<discontinued_date or enc.encounter_datetime<IFNULL(discontinued_date,'3000-01-01'))
            GROUP BY orders.patient_id,enc.encounter_datetime
            ) orders ON orders.patient_id=e.patient_id AND orders.encounter_datetime=e.encounter_datetime
    ) a GROUP BY a.patient_id ORDER BY a.patient_id, a.encounter_datetime
) f ON f.patient_id = b.patient_id
LEFT JOIN (
    SELECT
        patient_id,
        max(date_started) AS last_visit_date,
        min(date_started) AS first_visit_date
    FROM visit a
    WHERE a.voided = 0
    GROUP BY a.patient_id
    ORDER BY a.patient_id, date_started
) g ON g.patient_id = b.patient_id
LEFT JOIN (
    SELECT
        patient_id,
        min(if(b.name='Registration', a.encounter_datetime, null)) AS registration_date,
        min(if(b.name='HIV Enrollment', a.encounter_datetime, null)) AS hiv_enrolled_date,
        max(if(b.name='TB Enrollment', a.encounter_datetime, null)) AS tb_enrolled_date,
        max(if(b.name='MCH Child Enrollment', a.encounter_datetime, null)) AS mch_child_enrolled_date,
        max(if(b.name='MCH Mother Enrollment', a.encounter_datetime, null)) AS mch_mother_enrolled_date
    FROM encounter a
    LEFT JOIN encounter_type b ON b.encounter_type_id = a.encounter_type
    WHERE a.voided = 0 AND b.retired = 0
    GROUP BY a.patient_id
    ORDER BY a.patient_id, a.encounter_datetime
) h ON h.patient_id = b.patient_id
LEFT JOIN (
    SELECT
        a.person_id,
        lower(b.name) AS cause_of_death
    FROM person a
    INNER JOIN concept_name b ON b.concept_id = a.cause_of_death AND b.concept_name_type = 'FULLY_SPECIFIED' AND b.locale = 'en' AND b.voided = 0
) i ON i.person_id = b.patient_id
WHERE a.voided = 0 AND b.voided = 0 and a.birthdate between '1967-01-01' and '1997-12-31' and h.hiv_enrolled_date between '2011-01-01' and '2012-12-31' and a.gender = 'F'