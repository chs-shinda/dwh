<?php

header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');

Route::get('/', ['uses' => 'HomeController@index', 'name' => 'home.index']);
Route::get('/queue', ['uses' => 'HomeController@queue', 'name' => 'home.queue']);
Route::get('/post', ['uses' => 'HomeController@post', 'name' => 'home.post']);
Route::get('/status', ['uses' => 'HomeController@status', 'name' => 'home.status']);
Route::get('/process', ['uses' => 'HomeController@process', 'name' => 'home.process']);
Route::get('/clean', ['uses' => 'HomeController@clean', 'name' => 'home.clean']);

Route::get('/facilities', ['uses' => 'FacilityController@index', 'name' => 'facilities.index']);
Route::post('/facilities', ['uses' => 'FacilityController@store', 'name' => 'facilities.store']);
Route::post('/facilities/import', ['uses' => 'FacilityController@import', 'name' => 'facilities.import']);

Route::get('/emr-types', ['uses' => 'EmrTypeController@index', 'name' => 'emr_types.index']);
// Route::post('/emr-types', ['uses' => 'EmrTypeController@store', 'name' => 'emr_types.store']);

Route::get('/emr-backup-files', ['uses' => 'EmrBackupFileController@index', 'name' => 'emr_backup_files.index']);
Route::post('/emr-backup-files', ['uses' => 'EmrBackupFileController@store', 'name' => 'emr_backup_files.store']);

Route::get('/emr-data', ['uses' => 'EmrDataController@index', 'name' => 'emr_data.index']);
Route::get('/emr-data/fields', ['uses' => 'EmrDataController@fields', 'name' => 'emr_data.fields']);
Route::get('/emr-data/summaries', ['uses' => 'EmrDataController@summaries', 'name' => 'emr_data.summaries']);